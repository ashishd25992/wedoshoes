﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeDoShoesCMSAdminPanel.Models;
using WeDoShoesCMSAdminPanel.Util;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Services
{
    public class TaxService
    {
       private readonly WeDoShoesCMSHttpRequester _weDoShoesCMSHttpRequester;
        private readonly Logger _logger;

        public TaxService()
        {
            _weDoShoesCMSHttpRequester = new WeDoShoesCMSHttpRequester();
            _logger = LogManager.GetCurrentClassLogger();
        }

        internal TaxView GetTax()
        {
            Tax tax = _weDoShoesCMSHttpRequester.GetTax();
            if (tax == null)
                return null;
            TaxView taxView = new TaxView()
            {
                Id = tax.Id,
                TaxRate = tax.TaxRate
            };
            return taxView;
        }

        internal bool CreateTax(TaxView request)
        {
            Tax tax = new Tax()
            {
                TaxRate = request.TaxRate
            };

            return _weDoShoesCMSHttpRequester.CreateTax(tax);
        }

        internal bool UpdateTax(TaxView request)
        {
            Tax tax = new Tax()
            {
                TaxRate = request.TaxRate
            };

            return _weDoShoesCMSHttpRequester.UpdateTax(tax);            
        }
    }
}
