﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeDoShoesCMSAdminPanel.Models;
using WeDoShoesCMSAdminPanel.Networks;
using WeDoShoesCMSAdminPanel.Util;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Services
{
    public class ProductService
    {
        private readonly WeDoShoesCMSHttpRequester _weDoShoesCMSHttpRequester;
        private readonly Logger _logger;

        public ProductService()
        {
            _logger = LogManager.GetCurrentClassLogger();
            _weDoShoesCMSHttpRequester = new WeDoShoesCMSHttpRequester();
        }

        /// <summary>
        /// Get products list.
        /// </summary>
        /// <returns></returns>
        internal List<ViewModel.ProductView> ListProducts(int offset)
        {
            List<Product> products = _weDoShoesCMSHttpRequester.ListProduct(offset);
            if (products == null)
                return null;
            List<ProductView> productsView = new List<ProductView>();
            foreach (var product in products)
            {
                productsView.Add(new ProductView()
                    {
                        Id = product.Id,
                        CategoryId = product.CategoryId,
                        Name = product.Name,
                        Description = product.Description
                    });
            }
            return productsView;
        }

        /// <summary>
        /// Get products by category id.
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        internal List<ProductView> GetByCategoryId(int? categoryId, int? offset)
        {
            List<Product> products = _weDoShoesCMSHttpRequester.ProductByCategory(categoryId, offset);
            if (products == null)
                return null;
            List<ProductView> productsView = new List<ProductView>();
            foreach (var product in products)
            {
                productsView.Add(new ProductView()
                {
                    Id = product.Id,
                    CategoryId = product.CategoryId,
                    Name = product.Name,
                    Description = product.Description
                });
            }
            return productsView;
        }

        /// <summary>
        /// Create Product
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        internal bool CreateProduct(ProductView request)
        {
            Product product = new Product()
            {
                CategoryId = request.CategoryId,
                Name = request.Name,
                Description = request.Description,
            };
            product.Brands = new List<int>();
            for (int i = 0; i < request.SelectedBrandIds.Count; i++)
                product.Brands.Add(request.SelectedBrandIds[i]);
            product.SizeTypes = new List<int>();
            for (int i = 0; i < request.SelectedSizeIds.Count; i++)
                product.SizeTypes.Add(request.SelectedSizeIds[i]);

            return _weDoShoesCMSHttpRequester.CreateProduct(product);
        }

        /// <summary>
        /// Get Product Details By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal ProductView GetDetailsById(int id)
        {
            Product product = _weDoShoesCMSHttpRequester.ProductDetails(id);
            ProductView productView = new ProductView()
            {
                Id = product.Id,
                CategoryId = product.CategoryId,
                Name = product.Name,
                Description = product.Description,
                ImageUrl = product.ImageUrl
            };
            return productView;
        }

        /// <summary>
        /// Get all the services which isn't included in the product.
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="type"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        internal ProductView ListServiceByCategoryAndTypeNotInProduct(int categoryId, string type, int productId)
        {
            List<WeDoShoesService> listService = _weDoShoesCMSHttpRequester.ListServiceByTypeAndCategory(type, categoryId);
            if (listService == null)
                return null;
            ProductView productView = new ProductView()
            {
                Id = productId,
                CategoryId = categoryId
            };
            List<WeDoShoesService> listProductService = _weDoShoesCMSHttpRequester.ListServiceByProduct(productId);            
            if (listProductService == null)
            {
                foreach (var service in listService)
                {
                    productView.ListServiceView.Add(new ServiceView()
                    {
                        Id = service.Id,
                        Name = service.Name
                    });
                }
                return productView;
            }            
            var services = listService.Except(listProductService, new EqualityComparerForService());
            foreach (var service in services)
            {
                productView.ListServiceView.Add(new ServiceView()
                {
                    Id = service.Id,
                    Name = service.Name
                });
            }
            return productView;
        }

        /// <summary>
        /// Update product by id
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        internal bool UpdateProduct(ProductView request)
        {
            Product product = new Product()
            {
                CategoryId = request.CategoryId,
                Name = request.Name,
                Description = request.Description,
                ImageUrl = request.ImageUrl
            };

            return _weDoShoesCMSHttpRequester.UpdateProduct(product, request.Id);
        }

        /// <summary>
        /// Get selected size type of a product
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        internal ProductView GetSelectedSizeByProduct(int productId)
        {
            List<SizeType> listSizeOfAProduct = _weDoShoesCMSHttpRequester.ListSizeOfAProduct(productId);
            ProductView productView = new ProductView();
            if (listSizeOfAProduct == null)
            {
                productView.ServiceView.ListPriceView.Add(new PriceView()
                {
                    ProductSizeType = null,
                    ProductSizeTypeName = "NO_SIZE"
                });
                return productView;
            }
            if (listSizeOfAProduct != null)
            {
                foreach (var size in listSizeOfAProduct)
                {
                    productView.ServiceView.ListPriceView.Add(new PriceView()
                        {
                            ProductSizeType = size.Id,
                            ProductSizeTypeName = size.Name
                        });
                }
            }
            return productView;
        }

        /// <summary>
        /// List of service of a product.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        internal ProductView ListServiceOfProduct(int productId)
        {
            List<WeDoShoesService> listService = _weDoShoesCMSHttpRequester.ListServiceByProduct(productId);
            if (listService == null)
                return null;
            ProductView productView = new ProductView();
            foreach (var service in listService)
            {
                ServiceView serviceView = new ServiceView()
                {
                    Name = service.Name,
                    Description = service.Description
                };
                foreach (var price in service.ListPrice)
                {
                    serviceView.ListPriceView.Add(new PriceView()
                        {
                            ProductSizeType = price.ProductSizeType,
                            LabourCost = price.LabourCost,
                            MaterialCost = price.MaterialCost,
                            Profit = price.Profit,
                            AccessoryView = price.Accessory != null ? new AccessoryView()
                            {
                                Id = price.Accessory.Id,
                                Name = price.Accessory.Name,
                                Description = price.Accessory.Description,
                                AccessoryPriceView = price.Accessory.AccessoryPrice != null ? new AccessoryPriceView()
                                {
                                    AccessoryCost = price.Accessory.AccessoryPrice.AccessoryCost
                                } : null
                            } : null
                        });
                }
                productView.ListServiceView.Add(serviceView);
            }
            return productView;
        }

        internal List<AccessoryView> ListAccessoriesMinInfoByCategory(int categoryId)
        {
            List<Accessory> accessoryList = _weDoShoesCMSHttpRequester.ListAccessoriesMinInfoByCategory(categoryId);
            if (accessoryList == null)
                return null;
            List<AccessoryView> accessoryListView = new List<AccessoryView>();
            foreach (var accessory in accessoryList)
            {
                accessoryListView.Add(new AccessoryView()
                {
                    Id = accessory.Id,
                    Name = accessory.Name,
                });
            }
            return accessoryListView;
        }

        /// <summary>
        /// Add Service to a product
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        internal bool AddServiceToProduct(ProductView request)
        {
            WeDoShoesService service = new WeDoShoesService()
                {
                    Id = request.SelectedServiceId,
                    HasChildren = null,
                    ListSubService = null
                };
            if (request.ServiceView.Type == "REPAIR")
            {
                foreach (var price in request.ServiceView.ListPriceView)
                {
                    service.ListPrice.Add(new Price()
                        {
                            ProductSizeType = price.ProductSizeType,
                            LabourCost = price.LabourCost,
                            MaterialCost = price.MaterialCost,
                            Profit = price.Profit
                        });
                }
            }
            else
            {
                foreach (var price in request.ServiceView.ListPriceView)
                {
                    service.ListPrice.Add(new Price()
                    {
                        ProductSizeType = price.ProductSizeType,
                        Profit = price.Profit,
                        LabourCost = price.LabourCost,
                        Accessory = new Accessory()
                        {
                            Id = price.AccessoryView.Id
                        }
                    });
                }
            }
            List<WeDoShoesService> listService = new List<WeDoShoesService>();
            listService.Add(service);

            return _weDoShoesCMSHttpRequester.AddServiceToProduct(listService, request.Id);
        }

        /// <summary>
        /// Get list of sizes of a product.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        internal List<SizeTypeView> ListSizesOfAProduct(int productId)
        {
            List<SizeType> listSizes = _weDoShoesCMSHttpRequester.ListSizeOfAProduct(productId);
            if (listSizes == null)
                return null;
            List<SizeTypeView> listSizesView = new List<SizeTypeView>();
            foreach (var size in listSizes)
            {
                listSizesView.Add(new SizeTypeView()
                    {
                        Id = size.Id,
                        Name = size.Name
                    });
            }
            return listSizesView;
        }

        /// <summary>
        /// List brands of a product
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        internal List<BrandsView> ListBrandsOfAProduct(int? productId)
        {
            List<Brands> listBrands = _weDoShoesCMSHttpRequester.ListBrandByProduct(productId);
            if (listBrands == null)
                return null;
            List<BrandsView> listBrandsView = new List<BrandsView>();
            foreach (var brand in listBrands)
            {
                listBrandsView.Add(new BrandsView()
                {
                    Id = brand.Id,
                    Name = brand.Name
                });
            }
            return listBrandsView;
        }

        /// <summary>
        /// Get list of brands which isn't part of this product
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        internal ProductView ListOfBrandsNotInProduct(int productId, int? categoryId)
        {
            List<Brands> listBrand = _weDoShoesCMSHttpRequester.ListBrandsByCategory(categoryId);
            if (listBrand == null)
                return null;
            ProductView productView = new ProductView()
            {
                Id = productId,
                CategoryId = categoryId
            };
            var productBrands = _weDoShoesCMSHttpRequester.ListBrandByProduct(productId);
            if(productBrands == null)
            {
                foreach (var brand in listBrand)
                {
                    productView.ListBrandsView.Add(new BrandsView()
                    {
                        Id = brand.Id,
                        Name = brand.Name
                    });

                }
                return productView;
            }
            
            var brands = listBrand.Except(productBrands, new EqualityComparerForBrand());
            foreach (var brand in brands)
            {
                productView.ListBrandsView.Add(new BrandsView()
                    {
                        Id = brand.Id,
                        Name = brand.Name
                    });

            }
            return productView;
        }

        /// <summary>
        /// Add brands into a product
        /// </summary>
        /// <param name="productView"></param>
        /// <returns></returns>
        internal bool AddBrandInAProduct(ProductView productView)
        {
            return _weDoShoesCMSHttpRequester.AddBrandInProduct(productView.SelectedBrandIds, productView.Id);
        }

        /// <summary>
        /// Remove brands from a product.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        internal bool RemoveBrandFromProduct(int id, int productId)
        {
            return _weDoShoesCMSHttpRequester.RemoveBrandFromProduct(id, productId);
        }

        /// <summary>
        /// Remove size from a product.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        internal bool RemoveSizeFromProduct(int id, int productId)
        {
            return _weDoShoesCMSHttpRequester.RemoveSizeFromProduct(id, productId);
        }

        /// <summary>
        /// List of sizes which isn't already present in product.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        internal ProductView ListOfSizesNotInProduct(int productId)
        {
            List<SizeType> listSize = _weDoShoesCMSHttpRequester.ListSize();
            if (listSize == null)
                return null;
            ProductView productView = new ProductView()
            {
                Id = productId
            };
            List<SizeType> listProductSize = _weDoShoesCMSHttpRequester.ListSizeOfAProduct(productId);
            if(listProductSize == null)
            {
                foreach (var size in listSize)
                {
                    productView.ListBrandsView.Add(new BrandsView()
                    {
                        Id = size.Id,
                        Name = size.Name
                    });

                }
                return productView;
            }
            var sizes = listSize.Except(listProductSize, new EqualityComparerForSize());
            foreach (var size in sizes)
            {
                productView.ListBrandsView.Add(new BrandsView()
                {
                    Id = size.Id,
                    Name = size.Name
                });

            }
            return productView;
        }
    }

    #region Helper class
    //Compare two brand object
    class EqualityComparerForBrand : IEqualityComparer<Brands>
    {
        public bool Equals(Brands x, Brands y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode(Brands obj)
        {
            unchecked
            {
                return obj.Id.GetHashCode();
            }
        }
    }

    //Compare two size object
    class EqualityComparerForSize : IEqualityComparer<SizeType>
    {
        public bool Equals(SizeType x, SizeType y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode(SizeType obj)
        {
            unchecked
            {
                return obj.Id.GetHashCode();
            }
        }
    }

    //Compare two service object
    class EqualityComparerForService : IEqualityComparer<WeDoShoesService>
    {
        public bool Equals(WeDoShoesService x, WeDoShoesService y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode(WeDoShoesService obj)
        {
            unchecked
            {
                return obj.Id.GetHashCode();
            }
        }
    }
    #endregion
}