﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeDoShoesCMSAdminPanel.Models;
using WeDoShoesCMSAdminPanel.Networks;
using WeDoShoesCMSAdminPanel.Util;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Services
{
    public class CategoryService
    {
        private readonly WeDoShoesCMSHttpRequester _weDoShoesCMSHttpRequester;
        private readonly Logger _logger;

        public CategoryService()
        {
            _logger = LogManager.GetCurrentClassLogger();
            _weDoShoesCMSHttpRequester = new WeDoShoesCMSHttpRequester();
        }

        /// <summary>
        /// Get List Of Categories.
        /// </summary>
        /// <returns></returns>
        internal List<CategoryView> GetCategories()
        {
            List<Category> categories = _weDoShoesCMSHttpRequester.ListCategory();
            if (categories == null)
                return null;
            List<CategoryView> categoriesForView = new List<CategoryView>();
            foreach (var category in categories)
            {
                categoriesForView.Add(new CategoryView()
                    {
                        Id = category.Id,
                        Name = category.Name,
                        Description = category.Description
                    });
            }
            return categoriesForView;
        }

        /// <summary>
        /// Get Category Summary
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal CategoryView GetCategoryById(int id)
        {
            Category category = _weDoShoesCMSHttpRequester.CategoryDetails(id);
            return new CategoryView()
            {
                Id = category.Id,
                Name = category.Name,
                Description = category.Description
            };
        }

        /// <summary>
        /// Update Category
        /// </summary>
        /// <param name="id"></param>
        /// <param name="categoryModel"></param>
        /// <returns></returns>
        internal bool UpdateCategoryById(int id, CategoryView categoryModel)
        {
            Category category = new Category()
            {
                Name = categoryModel.Name,
                Description = categoryModel.Description
            };
            return _weDoShoesCMSHttpRequester.UpdateCategory(category, id);
        }

        /// <summary>
        /// Delete Category
        /// </summary>
        /// <param name="id"></param>
        /// <param name="categoryView"></param>
        /// <returns></returns>
        internal bool DeleteCategoryById(int id, CategoryView categoryView)
        {
            return _weDoShoesCMSHttpRequester.DeleteCategory(id);
        }

        /// <summary>
        /// Create Category.
        /// </summary>
        /// <param name="categoryView"></param>
        /// <returns></returns>
        internal bool CreateCategory(CategoryView categoryView)
        {
            Category category = new Category()
            {
                Name = categoryView.Name,
                Description = categoryView.Description
            };
            return _weDoShoesCMSHttpRequester.CreateCategory(category);
        }

        /// <summary>
        /// Get list for categories.
        /// </summary>
        /// <returns></returns>
        internal object ListCategory()
        {
            List<Category> categories = _weDoShoesCMSHttpRequester.ListCategory();
            if (categories == null)
                return null;
            List<CategoryView> listCategoryView = new List<CategoryView>();
            foreach (var category in categories)
            {
                listCategoryView.Add(new CategoryView()
                {
                    Id = category.Id,
                    Name = category.Name,
                });
            }
            return listCategoryView;
        }

        /// <summary>
        /// List of category for filter records.
        /// </summary>
        /// <returns></returns>
        internal object ListCategoryForFilter()
        {
            List<CategoryView> listCategoryView = new List<CategoryView>();
            listCategoryView.Add(new CategoryView()
            {
                Id = 0,
                Name = "By Category",
            });
            var categories = _weDoShoesCMSHttpRequester.ListCategory();
            if (categories != null)
            {
                foreach (var category in categories)
                {
                    listCategoryView.Add(new CategoryView()
                    {
                        Id = category.Id,
                        Name = category.Name,
                    });
                }
            }
            return listCategoryView;
        }
    }
}