﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeDoShoesCMSAdminPanel.Models;
using WeDoShoesCMSAdminPanel.Util;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Services
{
    public class FaqService
    {
        private readonly WeDoShoesCMSHttpRequester _weDoShoesCMSHttpRequester;
        private readonly Logger _logger;

        public FaqService()
        {
            _weDoShoesCMSHttpRequester = new WeDoShoesCMSHttpRequester();
            _logger = LogManager.GetCurrentClassLogger();
        }

        internal List<FaqCategoryView> ListFaqCategory()
        {
            List<FaqCategory> ListFaqCategory = _weDoShoesCMSHttpRequester.ListFaqCategory();
            if (ListFaqCategory == null)
                return null;
            List<FaqCategoryView> listFaqCategoryView = new List<FaqCategoryView>();
            foreach (var contactMode in ListFaqCategory)
            {
                listFaqCategoryView.Add(new FaqCategoryView()
                {
                    Id = contactMode.Id,
                    Name = contactMode.Name
                });
            }

            return listFaqCategoryView;
        }

        internal List<FaqView> ListFaqByCategoryId(int id)
        {
            List<Faq> ListFaq = _weDoShoesCMSHttpRequester.ListFaq(id);
            List<FaqView> listFaqView = new List<FaqView>();
            foreach (var faq in ListFaq)
            {
                listFaqView.Add(new FaqView()
                {
                    Id=faq.Id,
                    Question=faq.Question,
                    Answer=faq.Answer,
                    CategoryId=faq.CategoryId
                });
            }

            return listFaqView;
        }

        internal bool CreateFaqCategory(FaqCategoryView request)
        {
            FaqCategory faqCategory = new FaqCategory()
            {
                Name = request.Name
            };

            return _weDoShoesCMSHttpRequester.CreateFaqCategory(faqCategory);
        }

        internal FaqCategoryView FaqCategoryById(int id)
        {
            FaqCategory faqCategory = _weDoShoesCMSHttpRequester.FaqCategoryDetails(id);
            FaqCategoryView faqCategoryView = new FaqCategoryView()
            {
                Id = faqCategory.Id,
                Name = faqCategory.Name
            };
            return faqCategoryView;
        }

        internal bool UpdateFaqCategory(FaqCategoryView request)
        {
            FaqCategory faqCategory = new FaqCategory()
            {
                Name = request.Name
            };

            return _weDoShoesCMSHttpRequester.UpdateFaqCategory(request.Id, faqCategory);            
        }

        internal bool DeleteFaqCategory(FaqCategoryView request)
        {
            throw new NotImplementedException();
        }

        internal bool CreateFaq(FaqView request)
        {
            Faq faq = new Faq()
            {
                Question=request.Question,
                Answer=request.Answer,
                CategoryId=request.CategoryId,
            };

            return _weDoShoesCMSHttpRequester.CreateFaq(faq);
        }

        internal bool UpdateFaq(FaqView request)
        {
            Faq faq = new Faq()
            {
                Question = request.Question,
                Answer = request.Answer,
                CategoryId = request.CategoryId,
            };

            return _weDoShoesCMSHttpRequester.UpdateFaq(request.Id, faq);
        }

        internal bool DeleteFaq(FaqView request)
        {
            throw new NotImplementedException();
        }
    }
}
