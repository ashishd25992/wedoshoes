﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using WeDoShoesCMSAdminPanel.Models;
using WeDoShoesCMSAdminPanel.Util;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Services
{
    public class AccountService : Page
    {
        private readonly Logger _logger;
        private readonly WeDoShoesCoreHttpRequester _weDoShoesCoreHttpRequester;
        private readonly WeDoShoesApplication _weDoShoesApplication;

        public AccountService()
        {
            _logger = LogManager.GetCurrentClassLogger();
            _weDoShoesCoreHttpRequester = new WeDoShoesCoreHttpRequester();
            _weDoShoesApplication = new WeDoShoesApplication();
        }

        //Login
        internal bool Login(LoginView loginView)
        {
            var loginResponse = _weDoShoesCoreHttpRequester.Login(loginView.MobileNo, loginView.Password);
            if (loginResponse != null)
                return true;
            return false;
        }

        //Logout
        internal bool Logout()
        {
            return _weDoShoesCoreHttpRequester.Logout();
        }
    }
}