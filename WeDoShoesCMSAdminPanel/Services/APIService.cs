﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeDoShoesCMSAdminPanel.Models;
using WeDoShoesCMSAdminPanel.Util;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Services
{
    public class APIService
    {
        private readonly WeDoShoesCoreHttpRequester _weDoShoesCoreHttpRequester; 
        public APIService()
        {
            _weDoShoesCoreHttpRequester = new WeDoShoesCoreHttpRequester();
        }

        //List of apis.
        internal List<APIView> ListAPI(int offset)
        {
            List<API> apiList = _weDoShoesCoreHttpRequester.ListAPI(offset);
            if (apiList == null)
                return null;
            List<APIView> apiListView = new List<APIView>();
            foreach(var api in apiList)
            {
                apiListView.Add(new APIView()
                    {
                        Id = api.Id,
                        Name = api.Name,
                        DisplayName = api.DisplayName,
                        Description = api.Description,
                        Category = api.Category
                    });
            }
            return apiListView;
        }

        internal APIView APIDetails(int id)
        {
            API api = _weDoShoesCoreHttpRequester.APIDetails(id);
            if (api == null)
                return null;
            APIView apiView = new APIView()
            {
                Id = api.Id,
                Name = api.Name,
                DisplayName = api.DisplayName,
                Description = api.Description,
                Category = api.Category
            };
            return apiView;
        }

        internal bool CreateAPI(APIView request)
        {
            API api = new API()
            {
                Name = request.Name,
                DisplayName = request.DisplayName,
                Description = request.Description,
                Category = request.Category
            };

            return _weDoShoesCoreHttpRequester.CreateAPI(api);
        }

        //Update API
        internal bool UpdateAPI(int id, APIView request)
        {
            API api = new API()
            {
                Name = request.Name,
                DisplayName = request.DisplayName,
                Description = request.Description,
                Category = request.Category
            };

            return _weDoShoesCoreHttpRequester.UpdateAPI(id, api);
        }
        //TODO:
        internal bool DeleteAPI(APIView request)
        {
            throw new NotImplementedException();
        }        

        //List of apis by user type
        internal List<APIView> ListOfAPIByUserType(int id, int? offset)
        {
            List<API> listAPI = _weDoShoesCoreHttpRequester.ListOfAPIByUserType(id, offset);
            if (listAPI == null)
                return null;
            List<APIView> apis = new List<APIView>();
            foreach (var api in listAPI)
            {
                apis.Add(new APIView()
                {
                    Id = api.Id,
                    DisplayName = api.DisplayName
                });
            }
            return apis;
        }

        internal List<APIView> ListAPINotInUserType(int? id)
        {
            List<API> apis = _weDoShoesCoreHttpRequester.ListAPINotInUserType(id);
            if (apis == null)
                return null;
            List<APIView> listAPIS = new List<APIView>();
            foreach(var api in apis)
            {
                listAPIS.Add(new APIView()
                    {
                        Id = api.Id,
                        Name = api.DisplayName
                    });
            }
            return listAPIS;
        }

        //Add API for permission
        internal bool AddAPI(APIView request)
        {
            API api = new API();
            api.SelectedAPIIds = new List<int>();
            for (int i = 0; i < request.SelectedAPIIds.Count; i++)
                api.SelectedAPIIds.Add(request.SelectedAPIIds[i]);
            return _weDoShoesCoreHttpRequester.AddAPIPermission(request.Id, api);
        }

        //Revoke API permission
        internal bool RevokeAPIPermission(int userTypeId, int? apiId)
        {
            return _weDoShoesCoreHttpRequester.RevokeAPIPermission(userTypeId, apiId);
        }
    }
}