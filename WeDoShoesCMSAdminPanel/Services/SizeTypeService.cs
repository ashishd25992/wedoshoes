﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeDoShoesCMSAdminPanel.Models;
using WeDoShoesCMSAdminPanel.Networks;
using WeDoShoesCMSAdminPanel.Util;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Services
{
    public class SizeTypeService
    {
        private readonly WeDoShoesCMSHttpRequester _weDoShoesCMSHttpRequester;
        private readonly Logger _logger;

        public SizeTypeService()
        {
            _weDoShoesCMSHttpRequester = new WeDoShoesCMSHttpRequester();
            _logger = LogManager.GetCurrentClassLogger();
        }

        /// <summary>
        /// List of SizeType.
        /// </summary>
        /// <returns></returns>
        internal List<SizeTypeView> ListSizeType()
        {
            List<SizeType> listSizeType = _weDoShoesCMSHttpRequester.ListSize();
            List<SizeTypeView> listSizeTypeView = new List<SizeTypeView>();
            foreach (var sizeType in listSizeType)
            {
                if (sizeType.Name != "NO_SIZE")
                {
                    listSizeTypeView.Add(new SizeTypeView()
                        {
                            Id = sizeType.Id,
                            Name = sizeType.Name
                        });
                }
            }
            return listSizeTypeView;
        }

        /// <summary>
        /// SizeType by Id.
        /// </summary>
        /// <returns></returns>
        internal SizeTypeView SizeTypeById(int id)
        {            
            SizeType sizeType = _weDoShoesCMSHttpRequester.SizeDetails(id);
            SizeTypeView sizeTypeView = new SizeTypeView()
                {
                    Id = sizeType.Id,
                    Name = sizeType.Name
                };
            return sizeTypeView;
        }

        /// <summary>
        /// Create SizeType.
        /// </summary>
        /// <returns></returns>
        internal bool CreateSizeType(SizeTypeView request)
        {            
            SizeType sizeType = new SizeType()
            {
                Name = request.Name
            };

            return _weDoShoesCMSHttpRequester.CeateSize(sizeType);            
        }

        /// <summary>
        /// Update SizeType.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        internal bool UpdateSizeType(SizeTypeView request)
        {            
            SizeType sizeType = new SizeType()
            {
                Name = request.Name
            };

            return _weDoShoesCMSHttpRequester.UpdateSize(request.Id, sizeType);            
        }

        internal bool DeleteSizeType(SizeTypeView request)
        {
            throw new NotImplementedException();
        }
    }
}