﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeDoShoesCMSAdminPanel.Models;
using WeDoShoesCMSAdminPanel.Networks;
using WeDoShoesCMSAdminPanel.Util;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Services
{
    public class AccessoriesService
    {        
        private readonly Logger _logger;
        private readonly WeDoShoesCMSHttpRequester _weDoShoesCMSHttpRequester;
        
        public AccessoriesService()
        {
            _logger = LogManager.GetCurrentClassLogger();
            _weDoShoesCMSHttpRequester = new WeDoShoesCMSHttpRequester();
        }

        /// <summary>
        /// Get List of Accessories.
        /// </summary>
        /// <returns></returns>
        internal List<AccessoryView> ListAccessories(int offset)
        {
            List<Accessory> listOfAccessories = _weDoShoesCMSHttpRequester.ListAccessories(offset);
            if (listOfAccessories == null)
                return null;
            List<AccessoryView> listAccessoriesView = new List<AccessoryView>();
            foreach (var accessory in listOfAccessories)
            {
                listAccessoriesView.Add(new AccessoryView()
                {
                    Id = accessory.Id,
                    CategoryId = accessory.CategoryId,
                    Name = accessory.Name,
                    Description = accessory.Description
                });
            }
            return listAccessoriesView;
        }

        /// <summary>
        /// Get Accessory By Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal AccessoryView GetAccessoryById(int id)
        {
            Accessory accessory = _weDoShoesCMSHttpRequester.AccessoryDetails(id);            
            AccessoryView accessoryView = new AccessoryView()
            {
                Id = accessory.Id,
                CategoryId = accessory.CategoryId,
                Name = accessory.Name,
                Description = accessory.Description,
                SizeType = accessory.SizeType,
                AccessoryPriceView = new AccessoryPriceView()
                {
                    AccessoryCost = accessory.AccessoryPrice.AccessoryCost
                }
            };
            return accessoryView;
        }

        /// <summary>
        /// Create Accessory.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        internal bool CreateAccessory(AccessoryView request)
        {            
            Accessory accessory = new Accessory()
            {
                CategoryId = request.CategoryId,
                Name = request.Name,
                Description = request.Description,
                SizeType = request.SizeType != -1 ? request.SizeType : null,
                AccessoryPrice = new AccessoryPrice()
                {
                    AccessoryCost = request.AccessoryPriceView.AccessoryCost
                }
            };
            return _weDoShoesCMSHttpRequester.CreateAccessory(accessory);            
        }

        /// <summary>
        /// Update Accessory.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        internal bool UpdateAccessory(AccessoryView request)
        {            
            Accessory accessory = new Accessory()
            {
                CategoryId = request.CategoryId,
                Name = request.Name,
                Description = request.Description,
                SizeType = request.SizeType
            };
            return _weDoShoesCMSHttpRequester.UpdateAccessory(accessory, request.Id);       
        }

        /// <summary>
        /// Delete Accessory.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        internal bool DeleteAccessory(AccessoryView request)
        {
            return _weDoShoesCMSHttpRequester.DeleteAccessory(request);
        }

        /// <summary>
        /// Getting List of accessories by category id.
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        internal List<AccessoryView> ListAccessoriesByCategoryId(int? categoryId, int offset)
        {
            List<Accessory> accessoryList = _weDoShoesCMSHttpRequester.ListAccessoriesByCategory(categoryId, offset);
            if (accessoryList == null)
                return null;
            List<AccessoryView> accessoryListView = new List<AccessoryView>();
            foreach (var accessory in accessoryList)
            {
                accessoryListView.Add(new AccessoryView()
                    {
                        Id = accessory.Id,
                        CategoryId = accessory.CategoryId,
                        Name = accessory.Name,
                        Description = accessory.Description
                    });
            }
            return accessoryListView;
        }

        /// <summary>
        /// Update cost.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        internal bool UpdateCost(AccessoryView request)
        {            
            AccessoryPrice accessoryPrice = new AccessoryPrice()
            {
                AccessoryCost = request.AccessoryPriceView.AccessoryCost
            };
            return _weDoShoesCMSHttpRequester.UpdateCost(accessoryPrice, request.Id);            
        }        
    }
}