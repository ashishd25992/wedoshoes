﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeDoShoesCMSAdminPanel.Models;
using WeDoShoesCMSAdminPanel.Networks;
using WeDoShoesCMSAdminPanel.Util;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Services
{
    public class ServiceService
    {
        private readonly WeDoShoesCMSHttpRequester _weDoShoesCMSHttpRequester;
        private readonly Logger _logger;
        public ServiceService()
        {
            _weDoShoesCMSHttpRequester = new WeDoShoesCMSHttpRequester();
            _logger = LogManager.GetCurrentClassLogger();
        }

        /// <summary>
        /// Get All the services
        /// </summary>
        /// <returns></returns>
        internal List<ServiceView> GetAllServices()
        {
            List<WeDoShoesService> services = _weDoShoesCMSHttpRequester.ListRootServices();
            if (services == null)
                return null;
            List<ServiceView> listServices = new List<ServiceView>();            
            foreach (var service in services)
            {
                listServices.Add(new ServiceView()
                    {
                        Id = service.Id,
                        CategoryId = service.CategoryId,
                        Name = service.Name,
                        Acronym = service.Acronym,
                        Description = service.Description,
                        Type = service.Type,
                        HasChildren = (bool)service.HasChildren
                    });
            }
            return listServices;
        }

        /// <summary>
        /// Get Service by category id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal List<ServiceView> GetByCategoryId(int id)
        {
            List<WeDoShoesService> services = _weDoShoesCMSHttpRequester.ListRootServiceByCategory(id);
            if (services == null)
                return null;
            List<ServiceView> listServices = new List<ServiceView>();            
            foreach (var service in services)
            {
                listServices.Add(new ServiceView()
                {
                    Id = service.Id,
                    CategoryId = service.CategoryId,
                    Name = service.Name,
                    Acronym = service.Acronym,
                    Description = service.Description,
                    Type = service.Type,
                    HasChildren = (bool)service.HasChildren
                });
            }
            return listServices;
        }

        /// <summary>
        /// Get sub-services
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal List<SubServiceView> GetSubServicesByService(int? id, int count)
        {
            List<WeDoShoesService> services = _weDoShoesCMSHttpRequester.ListSubServiceByService(id, count);
            if (services == null)
                return null;
            List<SubServiceView> serviceList = new List<SubServiceView>();
            foreach (var service in services)
            {
                serviceList.Add(new SubServiceView()
                {
                    Id = service.Id,
                    CategoryId = service.CategoryId,
                    ParentId = service.ParentId,
                    Name = service.Name,
                    Acronym = service.Acronym,
                    Description = service.Description
                });
            }
            return serviceList;
        }

        /// <summary>
        /// Get service details by Service Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal ServiceView RootServiceDetails(int id)
        {
            WeDoShoesService service = _weDoShoesCMSHttpRequester.RootServiceDetails(id);
            ServiceView viewService = new ServiceView()
                {
                    Id = service.Id,
                    CategoryId = service.CategoryId,
                    Name = service.Name,
                    Acronym = service.Acronym,
                    Description = service.Description,
                    Type = service.Type,
                    HasChildren = (bool)service.HasChildren,
                    ProcessingTimeView = service.ProcessingTime != null ? new ProcessingTimeView()
                    {
                        ExpressProcessingDays = service.ProcessingTime.ExpressProcessingDays,
                        NormalProcessingDays = service.ProcessingTime.NormalProcessingDays
                    } : null,

                    ConvenienceChargesView = service.ConvenienceCharges != null ? new ConvenienceChargesView()
                    {
                        ConvenienceCharge = service.ConvenienceCharges.ConvenienceCharge,
                        //IsCurrentPrice = service.ConvenienceCharges.IsCurrentPrice,
                        MultiOrderExpressProcessingChargePerOrder = service.ConvenienceCharges.MultiOrderExpressProcessingChargePerOrder,
                        PackingCharge = service.ConvenienceCharges.PackingCharge,
                        SingleOrderExpressProcessingCharge = service.ConvenienceCharges.SingleOrderExpressProcessingCharge
                    } : null
                };
            return viewService;
        }

        /// <summary>
        /// Update root service
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        internal bool UpdateRootServiceById(int id, ServiceView request)
        {
            WeDoShoesService service = new WeDoShoesService()
            {
                CategoryId = request.CategoryId,
                Name = request.Name,
                Acronym = request.Acronym,
                Description = request.Description,
                HasChildren = request.HasChildren,
                Type = request.Type,
                ParentId = request.ParentId,
                ProcessingTime = new ProcessingTime(),
                ConvenienceCharges = new ConvenienceCharges()
            };
            return _weDoShoesCMSHttpRequester.UpdateRootService(id, service);
        }

        /// <summary>
        /// Create New Root Service
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        internal bool CreateRootService(ServiceView request)
        {
            WeDoShoesService service = new WeDoShoesService()
            {
                CategoryId = request.CategoryId,
                Name = request.Name,
                Acronym = request.Acronym,
                HasChildren = true,
                Type = "ROOT",
                Description = request.Description,
                ListPrice = null,
                ListSubService = null,
                ProcessingTime = request.ProcessingTimeView != null ? new ProcessingTime()
                {
                    NormalProcessingDays = request.ProcessingTimeView.NormalProcessingDays,
                    ExpressProcessingDays = request.ProcessingTimeView.ExpressProcessingDays
                } : null,
                ConvenienceCharges = request.ConvenienceChargesView != null ? new ConvenienceCharges()
                {
                    ConvenienceCharge = request.ConvenienceChargesView.ConvenienceCharge,
                    IsCurrentPrice = request.ConvenienceChargesView.IsCurrentPrice,
                    MultiOrderExpressProcessingChargePerOrder = request.ConvenienceChargesView.MultiOrderExpressProcessingChargePerOrder,
                    SingleOrderExpressProcessingCharge = request.ConvenienceChargesView.SingleOrderExpressProcessingCharge,
                    PackingCharge = request.ConvenienceChargesView.PackingCharge
                } : null
            };

            return _weDoShoesCMSHttpRequester.CreateRootService(service);
        }

        /// <summary>
        /// Delete root service.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal bool DeleteRootService(int id)
        {
            return _weDoShoesCMSHttpRequester.DeleteRootService(id);
        }

        /// <summary>
        /// Sub service details by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal SubServiceView GetSubServiceDetailsById(int id)
        {
            WeDoShoesService service = _weDoShoesCMSHttpRequester.SubServiceDetails(id);
            SubServiceView subServiceView = new SubServiceView()
            {
                Id = service.Id,
                CategoryId = service.CategoryId,
                ParentId = service.ParentId,
                Name = service.Name,
                Acronym = service.Acronym,
                Description = service.Description,
                Type = service.Type,
                HasChildren = service.HasChildren,
            };
            return subServiceView;
        }

        /// <summary>
        /// Update charges
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        internal bool UpdateCharges(int id, ServiceView request)
        {
            ConvenienceCharges convenienceCharges = new ConvenienceCharges()
            {
                ConvenienceCharge = request.ConvenienceChargesView.ConvenienceCharge,
                //IsCurrentPrice = request.ConvenienceChargesView.IsCurrentPrice,
                MultiOrderExpressProcessingChargePerOrder = request.ConvenienceChargesView.MultiOrderExpressProcessingChargePerOrder,
                SingleOrderExpressProcessingCharge = request.ConvenienceChargesView.SingleOrderExpressProcessingCharge,
                PackingCharge = request.ConvenienceChargesView.PackingCharge
            };
            return _weDoShoesCMSHttpRequester.UpdateCharges(id, convenienceCharges);
        }

        /// <summary>
        /// Update processing time.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        internal bool UpdateProcessingTime(int id, ServiceView request)
        {
            ProcessingTime processingTime = new ProcessingTime()
            {
                NormalProcessingDays = request.ProcessingTimeView.NormalProcessingDays,
                ExpressProcessingDays = request.ProcessingTimeView.ExpressProcessingDays
            };

            return _weDoShoesCMSHttpRequester.UpdateProcessingTime(id, processingTime);

        }

        /// <summary>
        /// Update subservice
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        internal bool UpdateSubServieById(SubServiceView request)
        {
            SubService subService = new SubService()
            {
                CategoryId = request.CategoryId,
                ParentId = request.ParentId,
                Name = request.Name,
                Acronym = request.Acronym,
                Description = request.Description,
                HasChildren = request.HasChildren,
                Type = request.Type
            };

            return _weDoShoesCMSHttpRequester.UpdateSubService(request.Id, subService);
        }

        /// <summary>
        /// Create Sub-Service
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        internal bool CreateSubServie(SubServiceView request)
        {
            SubService subService = new SubService()
            {
                CategoryId = request.CategoryId,
                ParentId = request.ParentId,
                Name = request.Name,
                Acronym = request.Acronym,
                Description = request.Description,
                Type = request.Type,
                HasChildren = false
            };

            return _weDoShoesCMSHttpRequester.CreateSubService(subService);
        }

        /// <summary>
        /// Get List Of Service Type.
        /// </summary>
        /// <returns></returns>
        internal object ServiceList()
        {
            List<ServiceView> serviceList = new List<ServiceView>()
            {
                new ServiceView()
                {
                    Type = "ROOT"                    
                }, 
                new ServiceView()
                {
                    Type = "REPAIR"
                },
                new ServiceView()
                {
                    Type = "REPLACE"
                }
            };
            return serviceList;
        }

        /// <summary>
        /// Sub-Service list
        /// </summary>
        /// <returns></returns>
        internal object SubServiceList()
        {
            List<ServiceView> serviceList = new List<ServiceView>()
            {                
                new ServiceView()
                {
                    Type = "REPAIR"
                },
                new ServiceView()
                {
                    Type = "REPLACE"
                }
            };
            return serviceList;
        }

        internal bool DeleteSubServieById(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Getting list of parent service by category id.
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        internal List<ServiceView> ParentServiceListByCategoryId(int? categoryId)
        {
            List<WeDoShoesService> serviceList = _weDoShoesCMSHttpRequester.ListParentServiceByCategory(categoryId);
            List<ServiceView> serviceViewList = new List<ServiceView>();
            if (serviceList == null)
            {
                serviceViewList.Add(new ServiceView()
                {
                    Name = "Select"
                });
                return serviceViewList;
            }
            serviceViewList.Add(new ServiceView()
            {
                Name = "Select"
            });
            if (serviceList != null)
            {
                foreach (var service in serviceList)
                {
                    serviceViewList.Add(new ServiceView()
                        {
                            Id = service.Id,
                            Name = service.Name
                        });
                }
            }
            return serviceViewList;
        }

        /// <summary>
        /// List of sub-service types
        /// </summary>
        /// <returns></returns>
        internal object SubServiceTypes()
        {
            List<ServiceView> serviceList = new List<ServiceView>()
            {    
                new ServiceView()
                {
                    Type = "--Select--"
                },
                new ServiceView()
                {
                    Type = "REPAIR"
                },
                new ServiceView()
                {
                    Type = "REPLACE"
                }
            };
            return serviceList;
        }

        /// <summary>
        /// Get services by root.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal List<ServiceView> GetServicesByRoot(int? id, int offset)
        {
            List<WeDoShoesService> listService = _weDoShoesCMSHttpRequester.ListServiceByRoot(id, offset);
            if (listService == null)
                return null;
            List<ServiceView> listSeviceView = new List<ServiceView>();            
            foreach (var service in listService)
            {
                listSeviceView.Add(new ServiceView()
                    {
                        Id = service.Id,
                        CategoryId = service.CategoryId,
                        ParentId = service.ParentId,
                        Name = service.Name,
                        Acronym = service.Acronym,
                        Description = service.Description,
                        Type = service.Type,
                        HasChildren = (bool)service.HasChildren
                    });
            }
            return listSeviceView;
        }

        #region SERVICE
        internal ServiceView ServiceDetails(int? id)
        {
            WeDoShoesService service = _weDoShoesCMSHttpRequester.ServiceDetails(id);
            ServiceView serviceView = new ServiceView()
            {
                Id = service.Id,
                CategoryId = service.CategoryId,
                ParentId = service.ParentId,
                Name = service.Name,
                Acronym = service.Acronym,
                Description = service.Description,
                Type = service.Type
            };
            return serviceView;
        }

        /// <summary>
        /// Create service
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        //internal bool CreateService(ServiceView request)
        //{
        //    WeDoShoesService service = new WeDoShoesService()
        //    {
        //        CategoryId = request.CategoryId,
        //        ParentId = request.ParentId,
        //        Name = request.Name,
        //        Acronym = request.Acronym,
        //        Description = request.Description,
        //        Type = request.Type,
        //        HasChildren = request.HasChildren,
        //        ListPrice = null
        //    };
        //    if (request.HasChildren == true)
        //        service.ListSubService.Add(new SubService()
        //            {
        //                Name = request.SubServiceView.Name,
        //                Acronym = request.SubServiceView.Acronym,
        //                Description = request.SubServiceView.Description,
        //                HasChildren = null
        //            });
        //    else
        //        service.ListSubService = null;

        //    return _weDoShoesCMSHttpRequester.CreateService(service);
        //}

        internal bool CreateService(ServiceView request)
        {
            WeDoShoesService service = new WeDoShoesService()
            {
                CategoryId = request.CategoryId,
                ParentId = request.ParentId,
                Name = request.Name,
                Acronym = request.Acronym,
                Description = request.Description,
                Type = request.Type,
                HasChildren = request.HasChildren,
                ListPrice = null
            };
            if (request.HasChildren == true)
            {
                service.ListSubService.Add(new SubService()
                {
                    Name = request.SubServiceView.Name,
                    Acronym = request.SubServiceView.Acronym,
                    Description = request.SubServiceView.Description,
                    HasChildren = null
                });
                foreach(var item in request.ListSubServiceView)
                {
                    service.ListSubService.Add(new SubService()
                    {
                        Name = item.Name,
                        Acronym = item.Acronym,
                        Description = item.Description,
                        HasChildren = null
                    });  
                        
                        
                }
            }
            else
                service.ListSubService = null;

            return _weDoShoesCMSHttpRequester.CreateService(service);
        }

        /// <summary>
        /// Update service 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        internal bool UpdateServiceById(int id, ServiceView request)
        {
            WeDoShoesService service = new WeDoShoesService()
            {
                CategoryId = request.CategoryId,
                ParentId = request.ParentId,
                Name = request.Name,
                Acronym = request.Acronym,
                Description = request.Description,
                Type = request.Type,
                HasChildren = request.HasChildren,
                ListSubService = null,
                ListPrice = null
            };

            return _weDoShoesCMSHttpRequester.UpdateService(request.Id, service);
        }

        internal bool DeleteService(int id)
        {
            throw new NotImplementedException();
        }

        #endregion

        /// <summary>
        /// Getting Root service processing time
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal ServiceView GetRootServiceProcessingTime(int id)
        {
            WeDoShoesService service = _weDoShoesCMSHttpRequester.RootServiceProcessingTime(id);
            ServiceView viewService = new ServiceView()
            {
                Id = service.Id,
                ProcessingTimeView = service.ProcessingTime != null ? new ProcessingTimeView()
                {
                    ExpressProcessingDays = service.ProcessingTime.ExpressProcessingDays,
                    NormalProcessingDays = service.ProcessingTime.NormalProcessingDays
                } : null,
            };
            return viewService;
        }

        /// <summary>
        /// Root Service charges.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal ServiceView GetRootServiceCharges(int id)
        {
            WeDoShoesService service = _weDoShoesCMSHttpRequester.RootServiceCharges(id);
            ServiceView viewService = new ServiceView()
            {
                Id = service.Id,
                ConvenienceChargesView = service.ConvenienceCharges != null ? new ConvenienceChargesView()
                {
                    ConvenienceCharge = service.ConvenienceCharges.ConvenienceCharge,
                    //IsCurrentPrice = service.ConvenienceCharges.IsCurrentPrice,
                    MultiOrderExpressProcessingChargePerOrder = service.ConvenienceCharges.MultiOrderExpressProcessingChargePerOrder,
                    PackingCharge = service.ConvenienceCharges.PackingCharge,
                    SingleOrderExpressProcessingCharge = service.ConvenienceCharges.SingleOrderExpressProcessingCharge
                } : null
            };
            return viewService;
        }

        /// <summary>
        /// Update root service processing time.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        internal bool UpdateRootServiceProcessingTime(int id, ServiceView request)
        {
            ProcessingTime processingTime = new ProcessingTime()
            {
                NormalProcessingDays = request.ProcessingTimeView.NormalProcessingDays,
                ExpressProcessingDays = request.ProcessingTimeView.ExpressProcessingDays
            };

            return _weDoShoesCMSHttpRequester.UpdateRootServiceProcessingTime(id, processingTime);
        }

        /// <summary>
        /// Update root service charges.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        internal bool UpdateRootServiceCharges(int id, ServiceView request)
        {
            ConvenienceCharges convenienceCharges = new ConvenienceCharges()
            {
                ConvenienceCharge = request.ConvenienceChargesView.ConvenienceCharge,
                SingleOrderExpressProcessingCharge = request.ConvenienceChargesView.SingleOrderExpressProcessingCharge,
                MultiOrderExpressProcessingChargePerOrder = request.ConvenienceChargesView.MultiOrderExpressProcessingChargePerOrder,
                PackingCharge = request.ConvenienceChargesView.PackingCharge
            };
            return _weDoShoesCMSHttpRequester.UpdateRootServiceCharges(id, convenienceCharges);
        }
    }
}