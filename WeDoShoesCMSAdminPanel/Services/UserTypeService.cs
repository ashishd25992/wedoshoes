﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeDoShoesCMSAdminPanel.Models;
using WeDoShoesCMSAdminPanel.Util;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Services
{
    public class UserTypeService
    {
        private readonly WeDoShoesCoreHttpRequester _weDoShoesCoreHttpRequester;
        public UserTypeService()
        {
            _weDoShoesCoreHttpRequester = new WeDoShoesCoreHttpRequester();
        }

        //Get list of user type
        internal List<UserTypeView> ListUserType()
        {
            List<UserType> listUserType = _weDoShoesCoreHttpRequester.ListUserType();
            if (listUserType == null)
                return null;
            List<UserTypeView> userTypes = new List<UserTypeView>();
            foreach (var userType in listUserType)
            {
                userTypes.Add(new UserTypeView()
                    {
                        Id = userType.Id,
                        Name = userType.Name
                    });
            }
            return userTypes;
        }

        //Get user type details
        internal UserTypeView UserTypeDetails(int id)
        {
            UserType userType = _weDoShoesCoreHttpRequester.UserTypeDetails(id);
            if (userType == null)
                return null;
            UserTypeView userTypeView = new UserTypeView()
                {
                    Id = userType.Id,
                    Name = userType.Name
                };
            return userTypeView;
        }

        //Create user type
        internal bool CreateUserType(UserTypeView request)
        {
            UserType userType = new UserType()
            {
                Name = request.Name
            };
            return _weDoShoesCoreHttpRequester.CreateUserType(userType);
        }

        //Update user type
        internal bool UpdateUserType(UserTypeView request)
        {
            UserType userType = new UserType()
            {
                Name = request.Name
            };
            return _weDoShoesCoreHttpRequester.UpdateUserType(request.Id, userType);
        }

        internal bool DeleteUserType(UserTypeView request)
        {
            return _weDoShoesCoreHttpRequester.DeleteUserType(request.Id);
        }

        //List user type by api
        internal List<UserTypeView> ListUserTypeByAPI(int id)
        {
            List<UserType> listUserType = _weDoShoesCoreHttpRequester.ListUserTypeByAPI(id);
            if (listUserType == null)
                return null;
            List<UserTypeView> userTypes = new List<UserTypeView>();
            foreach (var userType in listUserType)
            {
                userTypes.Add(new UserTypeView()
                {
                    Id = userType.Id,
                    Name = userType.Name
                });
            }
            return userTypes;
        }

        internal List<UserTypeView> ListUserNotInAPI(int id)
        {
            List<UserType> listUserType = _weDoShoesCoreHttpRequester.ListUserNotInAPI(id);
            if (listUserType == null)
                return null;
            List<UserTypeView> userTypes = new List<UserTypeView>();
            foreach (var userType in listUserType)
            {
                userTypes.Add(new UserTypeView()
                    {
                        Id = userType.Id,
                        Name = userType.Name
                    });
            }
            return userTypes;
        }

        //Add user type
        internal bool AddUserType(UserTypeView request)
        {
            UserType userType = new UserType();
            userType.SelectedUserTypeIds = new List<int>();
            for (int i = 0; i < request.SelectedUserType.Count; i++)
                userType.SelectedUserTypeIds.Add(request.SelectedUserType[i]);
            return _weDoShoesCoreHttpRequester.AddUserType(request.Id, userType);
        }

        //Remove user permission
        internal bool RevokeUserPermission(int userTypeId, int? apiId)
        {
            return _weDoShoesCoreHttpRequester.RevokeUserPermission(userTypeId, apiId);
        }
    }
}