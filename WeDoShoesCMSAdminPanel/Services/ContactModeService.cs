﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeDoShoesCMSAdminPanel.Models;
using WeDoShoesCMSAdminPanel.Util;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Services
{
    public class ContactModeService
    {
        private readonly WeDoShoesCMSHttpRequester _weDoShoesCMSHttpRequester;
        private readonly Logger _logger;

        public ContactModeService()
        {
            _weDoShoesCMSHttpRequester = new WeDoShoesCMSHttpRequester();
            _logger = LogManager.GetCurrentClassLogger();
        }

        internal List<ViewModel.ContactModeView> ListContactMode()
        {
            List<ContactMode> listContactMode = _weDoShoesCMSHttpRequester.ListContactMode();
            if (listContactMode == null)
                return null;
            List<ContactModeView> listContactModeView = new List<ContactModeView>();
            foreach (var contactMode in listContactMode)
            {
                listContactModeView.Add(new ContactModeView()
                    {
                        Id = contactMode.Id,
                        Name = contactMode.Name
                    });
            }

            return listContactModeView;
        }

        internal bool CreateContactMode(ContactModeView request)
        {
            ContactMode contactMode = new ContactMode()
            {
                Name = request.Name
            };

            return _weDoShoesCMSHttpRequester.CreateContactMode(contactMode);
        }

        internal ContactModeView ContactModeById(int id)
        {
            ContactMode contactMode = _weDoShoesCMSHttpRequester.ContactModeDetails(id);
            ContactModeView contactModeView = new ContactModeView()
            {
                Id = contactMode.Id,
                Name = contactMode.Name
            };
            return contactModeView;
        }

        internal bool UpdateContactMode(ContactModeView request)
        {
            ContactMode contactMode = new ContactMode()
            {
                Name = request.Name
            };

            return _weDoShoesCMSHttpRequester.UpdateContactMode(request.Id, contactMode);            
        }

        internal bool DeleteContactMode(ContactModeView request)
        {
            throw new NotImplementedException();
        }
    }
}
