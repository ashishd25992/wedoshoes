﻿(function () {

    //code dealing with product index page loadMore ajaxCall and smooth scroll down
    $(function () {
        
        var loadCount = 1;
        var loading = $("#loading");
        $("#loadMore").on("click", function (e) {

            e.preventDefault();

            $(document).on({

                ajaxStart: function () {
                    loading.show();
                },
                ajaxStop: function () {
                    loading.hide();
                }
            });

            var url = $("#ajaxUrl").val();
            $.ajax({
                url: url,
                data: {
                    userTypeId: userTypeId,
                    size: loadCount * 20
                },
                cache: false,
                type: "POST",
                success: function (data) {
                    if (data.length !== 0) {
                        $('#APITable tbody').append(data).fadeIn(2000);
                    }
                    else {
                        $("#loadMore").hide().fadeOut(2000);
                    }
                },
                error: function (xhr, status, error) {
                    console.log(xhr.responseText);
                    $("#loadMore").hide().fadeOut(2000);
                }
            });

            loadCount = loadCount + 1;

        });

    });


})();