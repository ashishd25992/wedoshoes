﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WeDoShoesCMSAdminPanel.Filter;
using WeDoShoesCMSAdminPanel.Services;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Controllers
{
    [AuthorizeFilter]
    public class UserTypeController : Controller
    {
        private readonly UserTypeService _userTypeService;
        private readonly Logger _logger;
        public UserTypeController()
        {
            _userTypeService = new UserTypeService();
            _logger = LogManager.GetCurrentClassLogger();
        }
        // GET: UserType
        public ActionResult Index()
        {
            try
            {
                List<UserTypeView> userTypeList = _userTypeService.ListUserType();
                return View(userTypeList);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: UserType/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                UserTypeView userType = _userTypeService.UserTypeDetails(id);
                return View(userType);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: UserType/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserType/Create
        [HttpPost]
        public ActionResult Create(UserTypeView request)
        {
            try
            {
                bool isCreated = _userTypeService.CreateUserType(request);
                if (isCreated)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: UserType/Edit/5
        public ActionResult Edit(UserTypeView request)
        {
            return View(request);
        }

        // POST: UserType/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, UserTypeView request)
        {
            try
            {
                bool isUpdated = _userTypeService.UpdateUserType(request);
                if (isUpdated)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: UserType/Delete/5
        public ActionResult Delete(UserTypeView request)
        {
            return View(request);
        }

        // POST: UserType/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, UserTypeView request)
        {
            try
            {
                bool isDeleted = _userTypeService.DeleteUserType(request);
                if (isDeleted)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET UserType by API
        public ActionResult UserTypeByAPI(int id)
        {
            try
            {
                List<UserTypeView> listUserType = _userTypeService.ListUserTypeByAPI(id);
                ViewBag.id = id;
                return PartialView("_ListUserTypeByAPI", listUserType);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }       

        //GET:
        public ActionResult ListUserNotInAPI(int id)
        {
            try
            {
                List<UserTypeView> userTypes = _userTypeService.ListUserNotInAPI(id);
                return Json(userTypes, JsonRequestBehavior.AllowGet);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: 
        public ActionResult AddUserType(int id)
        {
            UserTypeView userType = new UserTypeView()
            {
                Id = id
            };
            return View(userType);
        }

        //POST:
        [HttpPost]
        public ActionResult AddUserType(int id, UserTypeView request)
        {
            try
            {
                bool isAdded = _userTypeService.AddUserType(request);
                if (isAdded)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //DELETE:
        public ActionResult RevokeUserPermission(int userTypeId, int? apiId)
        {
            try
            {
                bool isDeleted = _userTypeService.RevokeUserPermission(userTypeId, apiId);
                if (isDeleted)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }
    }
}
