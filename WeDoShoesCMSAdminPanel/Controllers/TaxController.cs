﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WeDoShoesCMSAdminPanel.Filter;
using WeDoShoesCMSAdminPanel.Services;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Controllers
{
    [AuthorizeFilter]
    public class TaxController : Controller
    {
        private readonly TaxService _taxService;
        private readonly Logger _logger;
        public TaxController()
        {
            _taxService = new TaxService();
            _logger = LogManager.GetCurrentClassLogger();
        }

        // GET: Tax
        public ActionResult Index()
        {
            try
            {
                TaxView tax = _taxService.GetTax();
                return View(tax);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Tax/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Tax/Create
        [HttpPost]
        public ActionResult Create(TaxView request)
        {
            if (!ModelState.IsValid)
                return View();
            try
            {
                bool isCreated = _taxService.CreateTax(request);
                if (isCreated)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch
            {
                return View("Error");
            }
        }

        // GET: Tax/Edit
        public ActionResult Edit()
        {
            try
            {
                TaxView tax = _taxService.GetTax();
                return View(tax);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // POST: Tax/Edit
        [HttpPost]
        public ActionResult Edit(TaxView request)
        {
            if (!ModelState.IsValid)
                return View(request);
            try
            {
                bool isUpdated = _taxService.UpdateTax(request);
                if (isUpdated)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch
            {
                return View("Error");
            }
        }

    }
}
