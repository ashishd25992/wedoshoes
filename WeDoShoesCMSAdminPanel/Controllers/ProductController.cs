﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WeDoShoesCMSAdminPanel.Filter;
using WeDoShoesCMSAdminPanel.Services;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Controllers
{
    [AuthorizeFilter]
    public class ProductController : Controller
    {
        private readonly ProductService _productService;
        private readonly Logger _logger;

        public ProductController()
        {
            _productService = new ProductService();
            _logger = LogManager.GetCurrentClassLogger();
        }
        // GET: Product
        public ActionResult Index()
        {
            try
            {
                List<ProductView> productList = _productService.ListProducts(0);
                return View(productList);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Products 
        public ActionResult ListByCategory(int? categoryId, int? size)
        {
            try
            {
                if (size == null)
                    size = 0;
                List<ProductView> productList = _productService.GetByCategoryId(categoryId, size);
                ViewBag.categoryid = categoryId;
                return View(productList);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Product/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                ProductView product = _productService.GetDetailsById(id);
                return View(product);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Product/GetById/5
        public ActionResult GetById(int id, int categoryId)
        {
            try
            {
                ProductView product = _productService.GetDetailsById(id);
                return View(product);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: Product/ListServiceOfProduct/5
        public ActionResult ListServiceOfProduct(int productId)
        {
            try
            {
                ProductView product = _productService.ListServiceOfProduct(productId);
                return PartialView("_ServicesOfAProduct", product);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: List Brands of a product
        public ActionResult ListBrandsByProduct(int? productId, int? categoryId)
        {
            try
            {
                List<BrandsView> brands = _productService.ListBrandsOfAProduct(productId);
                ViewData["productid"] = productId;
                ViewData["categoryid"] = categoryId;
                return PartialView("_BrandsByProduct", brands);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: Brands
        public ActionResult BrandsNotInProduct(int productId, int? categoryId)
        {
            try
            {
                ProductView listOfBrandsNotInProduct = _productService.ListOfBrandsNotInProduct(productId, categoryId);
                return View(listOfBrandsNotInProduct);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //Add Brand
        public ActionResult AddBrand(ProductView productView)
        {
            try
            {
                bool isAdded = _productService.AddBrandInAProduct(productView);
                if (isAdded)
                    return RedirectToAction("Details", new { id = productView.Id });
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //Remove Brand
        public ActionResult RemoveBrand(int id, int productId)
        {
            try
            {
                bool isDeleted = _productService.RemoveBrandFromProduct(id, productId);
                if (isDeleted)
                    return RedirectToAction("Details", new { id = productId });
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: List Sizes of a product
        public ActionResult ListSizesByProduct(int productId)
        {
            try
            {
                List<SizeTypeView> sizes = _productService.ListSizesOfAProduct(productId);
                ViewData["productid"] = productId;
                return PartialView("_SizesByProduct", sizes);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: List Sizes not in a product
        //TODO:
        public ActionResult SizesNotInProduct(int productId)
        {
            try
            {
                ProductView listOfSizesNotInProduct = _productService.ListOfSizesNotInProduct(productId);
                return View(listOfSizesNotInProduct);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //Remove size
        public ActionResult RemoveSize(int id, int productId)
        {
            try
            {
                bool isDeleted = _productService.RemoveSizeFromProduct(id, productId);
                if (isDeleted)
                    return RedirectToAction("Details", new { id = productId });
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: Services by category
        //TODO:
        public ActionResult ListOfServicesByType(int categoryId, string type, int productId, string selectedSizeIds)
        {
            try
            {
                ProductView serviceListMinInfo = _productService.ListServiceByCategoryAndTypeNotInProduct(categoryId, type, productId);
                return Json(serviceListMinInfo.ListServiceView, JsonRequestBehavior.AllowGet);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: Get selected size of a product
        public ActionResult PriceForSelectedSizeList(int categoryId, int productId, string type)
        {
            //productId = 3;
            try
            {
                ProductView selectedSizeListMinInfo = _productService.GetSelectedSizeByProduct(productId);
                if (type == "REPAIR")
                    return PartialView("_MaterialCostBySize", selectedSizeListMinInfo);
                else
                {
                    selectedSizeListMinInfo.CategoryId = categoryId;
                    List<AccessoryView> listAccessories = _productService.ListAccessoriesMinInfoByCategory(categoryId);
                    foreach (var accessory in listAccessories)
                    {
                        selectedSizeListMinInfo.ServiceView.PriceView.ListAccessoryView.Add(new AccessoryView()
                        {
                            Id = accessory.Id,
                            Name = accessory.Name
                        });
                    }
                    return PartialView("_AccessoryCostBySize", selectedSizeListMinInfo);
                }
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //POST: Add service to product
        [HttpPost]
        public ActionResult AddService(ProductView request)
        {
            //return View("Error");
            try
            {
                bool isAdded = _productService.AddServiceToProduct(request);
                if (isAdded)
                    return RedirectToAction("ListByCategory", new { categoryId = request.CategoryId });
                else
                {
                    return View("Error");
                }
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Product/Create
        public ActionResult Create(int? categoryId)
        {
            if (categoryId != null)
            {
                ProductView product = new ProductView()
                {
                    CategoryId = categoryId
                };
                return View(product);
            }
            return View();
        }

        // POST: Product/Create
        [HttpPost]
        public ActionResult Create(ProductView request)
        {
            if (!ModelState.IsValid)
                return View(request);
            try
            {
                bool isCreated = _productService.CreateProduct(request);
                if (isCreated)
                    return RedirectToAction("ListByCategory", new { categoryId = request.CategoryId });
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Product/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                ProductView product = _productService.GetDetailsById(id);
                return View(product);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // POST: Product/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ProductView request)
        {
            try
            {
                bool isUpdated = _productService.UpdateProduct(request);
                if (isUpdated)
                    return RedirectToAction("Details/" + request.CategoryId, "Category");
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                ProductView product = _productService.GetDetailsById(id);
                return View(product);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // POST: Product/Delete/5
        //TODO:
        [HttpPost]
        public ActionResult Delete(int id, ProductView request)
        {
            try
            {
                // TODO: Add delete logic here

                throw new NotImplementedException();
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //POST: Load more brands
        [HttpPost]
        public ActionResult LoadMoreProducts(int? categoryId, int size)
        {
            try
            {
                List<ProductView> listProducts = _productService.GetByCategoryId(categoryId, size);
                if (listProducts != null)
                {
                    return PartialView("_LoadMoreProduct", listProducts);
                }
                return Json(listProducts);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }
    }
}
