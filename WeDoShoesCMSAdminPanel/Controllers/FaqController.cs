﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WeDoShoesCMSAdminPanel.Filter;
using WeDoShoesCMSAdminPanel.Services;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Controllers
{
    [AuthorizeFilter]
    public class FaqController : Controller
    {
        private readonly FaqService _faqService;
        private readonly Logger _logger;

        public FaqController()
        {
            _faqService = new FaqService();
            _logger = LogManager.GetCurrentClassLogger();
        }

        #region FaqCategory
        // GET: Faq Category List
        public ActionResult Index()
        {
            try
            {
                List<FaqCategoryView> listContactMode = _faqService.ListFaqCategory();
                return View(listContactMode);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Faq/CategoryDetails/5
        public ActionResult CategoryDetails(int id)
        {
            try
            {
                ViewData["categoryid"] = id;
                List<FaqView> listFaq = _faqService.ListFaqByCategoryId(id);
                return View("CategoryDetails",listFaq);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Faq/CreateCategory
        public ActionResult CreateCategory()
        {
            return View();
        }

        // POST: Faq/CreateCategory
        [HttpPost]
        public ActionResult CreateCategory(FaqCategoryView request)
        {
            if (!ModelState.IsValid)
                return View();
            try
            {
                bool isCreated = _faqService.CreateFaqCategory(request);
                if (isCreated)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch
            {
                return View("Error");
            }
        }

        // GET: Faq/Edit/5
        public ActionResult EditCategory(int id)
        {
            try
            {
                FaqCategoryView faqCategory = _faqService.FaqCategoryById(id);
                return View(faqCategory);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // POST: Faq/Edit/5
        [HttpPost]
        public ActionResult EditCategory(int id, FaqCategoryView request)
        {
            if (!ModelState.IsValid)
                return View(request);
            try
            {
                bool isUpdated = _faqService.UpdateFaqCategory(request);
                if (isUpdated)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch
            {
                return View("Error");
            }
        }

        // GET: Faq/Delete/5
        public ActionResult DeleteCategory(int id)
        {
            try
            {
                FaqCategoryView faqCategory = _faqService.FaqCategoryById(id);
                return View(faqCategory);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // POST: Faq/Delete/5
        [HttpPost]
        public ActionResult DeleteCategory(int id, FaqCategoryView request)
        {
            try
            {
                bool isDeleted = _faqService.DeleteFaqCategory(request);
                if (isDeleted)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch
            {
                return View("Error");
            }
        }
        #endregion

        #region Faq

        // GET: Faq/Create
        public ActionResult Create(int categoryId)
        {
            ViewData["categoryid"] = categoryId;
            return View();
        }

        // POST: Faq/Create
        [HttpPost]
        public ActionResult Create(FaqView request)
        {
            if (!ModelState.IsValid)
                return View();
            try
            {
                bool isCreated = _faqService.CreateFaq(request);
                if (isCreated)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch
            {
                return View("Error");
            }
        }

        // GET: Faq/Edit/5
        public ActionResult Edit(FaqView request)
        {
            return View(request);
        }

        // POST: Faq/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FaqView request)
        {
            if (!ModelState.IsValid)
                return View(request);
            try
            {
                bool isUpdated = _faqService.UpdateFaq(request);
                if (isUpdated)
                    return RedirectToAction("CategoryDetails", request.CategoryId);
                return View("Error");
            }
            catch
            {
                return View("Error");
            }
        }

        // GET: Faq/Delete/5
        public ActionResult Delete(FaqView request)
        {
            return View(request);
        }

        // POST: Faq/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FaqView request)
        {
            try
            {
                bool isDeleted = _faqService.DeleteFaq(request);
                if (isDeleted)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch
            {
                return View("Error");
            }
        }
        #endregion
    }
}
