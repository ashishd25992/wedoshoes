﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WeDoShoesCMSAdminPanel.Services;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Controllers
{
    public class AccountController : Controller
    {
        private readonly AccountService _accountService;
        private readonly Logger _logger;

        public AccountController()
        {
            _accountService = new AccountService();
            _logger = LogManager.GetCurrentClassLogger();
        }
        // GET: User
        public ActionResult Login()
        {
            return View();
        }

        // Post: Login data
        [HttpPost]
        public ActionResult Login(LoginView login)
        {
            try
            {
                var isLoginSuccessfully = _accountService.Login(login);
                if (isLoginSuccessfully)
                    return RedirectToAction("Index", "Home");
                ModelState.AddModelError("", "The Phone or Password is incorrect.");
                return View(login);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        ModelState.AddModelError("", "The Phone or Password is incorrect.");
                        return View(login);
                    }
                }
                ModelState.AddModelError("", "Something went wrong, Please try again after sometime.");
                return View(login);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                ModelState.AddModelError("", "Something went wrong, Please try again after sometime.");
                return View(login);
            }
        }

        //Get: Logout
        public ActionResult Logout()
        {
            try
            {
                if (_accountService.Logout())
                    return RedirectToAction("Login", "Account");
                return RedirectToAction("Login", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: User/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}