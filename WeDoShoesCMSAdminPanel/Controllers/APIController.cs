﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WeDoShoesCMSAdminPanel.Filter;
using WeDoShoesCMSAdminPanel.Services;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Controllers
{
    [AuthorizeFilter]
    public class APIController : Controller
    {
        private readonly APIService _apiService;
        private readonly Logger _logger;

        public APIController()
        {
            _apiService = new APIService();
            _logger = LogManager.GetCurrentClassLogger();
        }
        // GET: API
        public ActionResult Index()
        {
            try
            {
                List<APIView> apiList = _apiService.ListAPI(0);
                return View(apiList);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult LoadMoreAPI(int size)
        {
            try
            {
                List<APIView> apiList = _apiService.ListAPI(size);
                if (apiList != null)
                {
                    return PartialView("_LoadMoreAPI", apiList);
                }
                return Json(apiList);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }
        // GET: API/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                APIView api = _apiService.APIDetails(id);
                return View(api);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: API/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: API/Create
        [HttpPost]
        public ActionResult Create(APIView request)
        {
            try
            {
                bool isCreated = _apiService.CreateAPI(request);
                if (isCreated)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: API/Edit/5
        public ActionResult Edit(APIView request)
        {
            return View(request);
        }

        // POST: API/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, APIView request)
        {
            try
            {
                bool isUpdated = _apiService.UpdateAPI(id, request);
                if (isUpdated)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: API/Delete/5
        public ActionResult Delete(APIView request)
        {
            return View(request);
        }

        // POST: API/Delete/5
        //TODO:
        [HttpPost]
        public ActionResult Delete(int id, APIView request)
        {
            try
            {
                bool isDeleted = _apiService.DeleteAPI(request);
                if (isDeleted)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: API by UserType
        public ActionResult APIByUserType(int id, int? size)
        {
            try
            {
                List<APIView> listOfAPIByUserType = _apiService.ListOfAPIByUserType(id, size);
                ViewBag.id = id;
                return PartialView("_ListOfAPIByUserType", listOfAPIByUserType);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult LoadMoreAPIByUserType(int userTypeId, int? size)
        {
            try
            {
                List<APIView> listOfAPIByUserType = _apiService.ListOfAPIByUserType(userTypeId, size);
                ViewBag.id = userTypeId;
                if (listOfAPIByUserType != null)
                {
                    return PartialView("_LoadMoreAPIByUserType", listOfAPIByUserType);
                }
                return Json(listOfAPIByUserType);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }
        //GET:
        public ActionResult ListAPINotInUserType(int? id)
        {
            try
            {
                List<APIView> apis = _apiService.ListAPINotInUserType(id);
                return Json(apis, JsonRequestBehavior.AllowGet);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: Manage permission
        public ActionResult AddAPI(int? id)
        {
            APIView api = new APIView()
            {
                Id = id
            };
            return View(api);
        }

        //POST: Manage permission
        [HttpPost]
        public ActionResult AddAPI(int id, APIView request)
        {
            try
            {
                bool isAdded = _apiService.AddAPI(request);
                if (isAdded)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //DELETE:
        public ActionResult RevokeAPIPermission(int userTypeId, int? apiId)
        {
            try
            {
                bool isDeleted = _apiService.RevokeAPIPermission(userTypeId, apiId);
                if (isDeleted)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }
    }
}
