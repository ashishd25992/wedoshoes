﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WeDoShoesCMSAdminPanel.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Error()
        {
            return View("Error");
        }

        // GET: Page Not Found
        public ActionResult PageNotFound()
        {
            return View("PageNotFound");
        }
    }
}