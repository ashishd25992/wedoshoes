﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WeDoShoesCMSAdminPanel.Filter;
using WeDoShoesCMSAdminPanel.Services;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Controllers
{
    [AuthorizeFilter]
    public class CategoryController : Controller
    {
        private readonly CategoryService _categoryService;
        private readonly Logger _logger;

        public CategoryController()
        {
            _categoryService = new CategoryService();
            _logger = LogManager.GetCurrentClassLogger();
        }

        // GET: Category
        [HandleError()]
        public ActionResult Index()
        {
            try
            {
                List<CategoryView> categories = _categoryService.GetCategories();
                return View(categories);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Category/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                CategoryView category = _categoryService.GetCategoryById(id);
                return View(category);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Category/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Category/Create
        [HttpPost]
        public ActionResult Create(CategoryView category)
        {            
            try
            {
                bool isCreated = _categoryService.CreateCategory(category);
                if (isCreated)
                    return RedirectToAction("Index");
                return RedirectToAction("Index");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Category/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                CategoryView category = _categoryService.GetCategoryById(id);
                return View(category);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // POST: Category/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, CategoryView category)
        {
            if (!ModelState.IsValid)
                return View(category);
            try
            {
                bool isUpdated = _categoryService.UpdateCategoryById(id, category);
                if (isUpdated)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Category/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                CategoryView category = _categoryService.GetCategoryById(id);
                return View(category);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // POST: Category/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, CategoryView categoryModel)
        {
            try
            {
                bool isDeleted = _categoryService.DeleteCategoryById(id, categoryModel);
                if (isDeleted)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: List category
        public ActionResult ListCategory()
        {
            try
            {
                var listCatgory = _categoryService.ListCategory();
                return Json(listCatgory, JsonRequestBehavior.AllowGet);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: List category
        public ActionResult ListCategoryForFilter()
        {
            try
            {
                var listCatgory = _categoryService.ListCategoryForFilter();
                return Json(listCatgory, JsonRequestBehavior.AllowGet);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }        
    }
}
