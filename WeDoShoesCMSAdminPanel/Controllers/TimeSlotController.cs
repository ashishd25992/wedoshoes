﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WeDoShoesCMSAdminPanel.Filter;
using WeDoShoesCMSAdminPanel.Services;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Controllers
{
    [AuthorizeFilter]
    public class TimeSlotController : Controller
    {
        private readonly TimeSlotService _timeSlotService;
        private readonly Logger _logger;

        public TimeSlotController()
        {
            _timeSlotService = new TimeSlotService();
            _logger = LogManager.GetCurrentClassLogger();
        }

        // GET: TimeSlot
        public ActionResult Index()
        {
            try
            {
                List<TimeSlotView> listTimeSlot = _timeSlotService.ListTimeSlot();
                return View(listTimeSlot);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: TimeSlot/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                TimeSlotView timeSlot = _timeSlotService.TimeSlotById(id);
                return View(timeSlot);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: TimeSlot/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TimeSlot/Create
        [HttpPost]
        public ActionResult Create(TimeSlotView request)
        {
            if (!ModelState.IsValid)
                return View();
            try
            {
                bool isCreated = _timeSlotService.CreateTimeSlot(request);
                if (isCreated)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch
            {
                return View("Error");
            }
        }

        // GET: TimeSlot/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                TimeSlotView timeSlot = _timeSlotService.TimeSlotById(id);
                return View(timeSlot);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // POST: TimeSlot/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, TimeSlotView request)
        {
            if (!ModelState.IsValid)
                return View(request);
            try
            {
                bool isUpdated = _timeSlotService.UpdateTimeSlot(request);
                if (isUpdated)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch
            {
                return View("Error");
            }
        }

        // GET: TimeSlot/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                TimeSlotView timeSlot = _timeSlotService.TimeSlotById(id);
                return View(timeSlot);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // POST: TimeSlot/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, TimeSlotView request)
        {
            try
            {
                bool isDeleted = _timeSlotService.DeleteTimeSlot(request);
                if (isDeleted)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch
            {
                return View("Error");
            }
        }
    }
}
