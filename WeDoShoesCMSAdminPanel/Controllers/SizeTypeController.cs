﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WeDoShoesCMSAdminPanel.Filter;
using WeDoShoesCMSAdminPanel.Services;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Controllers
{
    [AuthorizeFilter]
    public class SizeTypeController : Controller
    {
        private readonly SizeTypeService _sizeTypeService;
        private readonly Logger _logger;

        public SizeTypeController()
        {
            _sizeTypeService = new SizeTypeService();
            _logger = LogManager.GetCurrentClassLogger();
        }

        // GET: SizeType
        public ActionResult Index()
        {
            try
            {
                List<SizeTypeView> listSizeType = _sizeTypeService.ListSizeType();
                return View(listSizeType);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: SizeType
        public ActionResult ListSizeType()
        {
            try
            {
                List<SizeTypeView> listSizeType = _sizeTypeService.ListSizeType();                
                return Json(listSizeType, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: SizeType/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                SizeTypeView sizeType = _sizeTypeService.SizeTypeById(id);
                return View(sizeType);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: SizeType/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SizeType/Create
        [HttpPost]
        public ActionResult Create(SizeTypeView request)
        {
            if (!ModelState.IsValid)
                return View();
            try
            {
                bool isCreated = _sizeTypeService.CreateSizeType(request);
                if (isCreated)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch
            {
                return View("Error");
            }
        }

        // GET: SizeType/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                SizeTypeView sizeType = _sizeTypeService.SizeTypeById(id);
                return View(sizeType);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // POST: SizeType/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, SizeTypeView request)
        {
            if (!ModelState.IsValid)
                return View(request);
            try
            {
                bool isUpdated = _sizeTypeService.UpdateSizeType(request);
                if (isUpdated)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch
            {
                return View("Error");
            }
        }

        // GET: SizeType/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                SizeTypeView sizeType = _sizeTypeService.SizeTypeById(id);
                return View(sizeType);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // POST: SizeType/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, SizeTypeView request)
        {
            try
            {
                bool isDeleted = _sizeTypeService.DeleteSizeType(request);
                if (isDeleted)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch
            {
                return View("Error");
            }
        }
    }
}
