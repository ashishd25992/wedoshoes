﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WeDoShoesCMSAdminPanel.Filter;
using WeDoShoesCMSAdminPanel.Services;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Controllers
{
    [AuthorizeFilter]
    public class ContactModeController : Controller
    {
        private readonly ContactModeService _contactModeService;
        private readonly Logger _logger;

        public ContactModeController()
        {
            _contactModeService = new ContactModeService();
            _logger = LogManager.GetCurrentClassLogger();
        }
        // GET: ContactMode
        public ActionResult Index()
        {
            try
            {
                List<ContactModeView> listContactMode = _contactModeService.ListContactMode();
                return View(listContactMode);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: ContactMode/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                ContactModeView contactMode = _contactModeService.ContactModeById(id);
                return View(contactMode);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: ContactMode/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ContactMode/Create
        [HttpPost]
        public ActionResult Create(ContactModeView request)
        {
            if (!ModelState.IsValid)
                return View();
            try
            {
                bool isCreated = _contactModeService.CreateContactMode(request);
                if (isCreated)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch
            {
                return View("Error");
            }
        }

        // GET: ContactMode/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                ContactModeView contactMode = _contactModeService.ContactModeById(id);
                return View(contactMode);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // POST: ContactMode/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ContactModeView request)
        {
            if (!ModelState.IsValid)
                return View(request);
            try
            {
                bool isUpdated = _contactModeService.UpdateContactMode(request);
                if (isUpdated)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch
            {
                return View("Error");
            }
        }

        // GET: ContactMode/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                ContactModeView contactMode = _contactModeService.ContactModeById(id);
                return View(contactMode);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // POST: ContactMode/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, ContactModeView request)
        {
            try
            {
                bool isDeleted = _contactModeService.DeleteContactMode(request);
                if (isDeleted)
                    return RedirectToAction("Index");
                return View("Error");
            }
            catch
            {
                return View("Error");
            }
        }
    }
}
