﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WeDoShoesCMSAdminPanel.Filter;
using WeDoShoesCMSAdminPanel.Services;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Controllers
{
    [AuthorizeFilter]
    public class ServiceController : Controller
    {
        private readonly ServiceService _serviceService;
        private readonly Logger _logger;

        public ServiceController()
        {
            _serviceService = new ServiceService();
            _logger = LogManager.GetCurrentClassLogger();
        }

        # region ROOT SERVICE
        // GET: RootService
        public ActionResult Index()
        {
            try
            {
                List<ServiceView> services = _serviceService.GetAllServices();
                return View(services);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: RootService by category Id.
        public ActionResult ListByCategory(int categoryId)
        {
            try
            {
                List<ServiceView> services = _serviceService.GetByCategoryId(categoryId);
                ViewData["categoryid"] = categoryId;
                return View(services);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: Root service details by Id.
        public ActionResult RootServiceDetails(int id)
        {
            try
            {
                ServiceView rootService = _serviceService.RootServiceDetails(id);
                return View(rootService);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: Root service details by Id.
        public ActionResult EditRootService(ServiceView request)
        {
            return View(request);
        }

        //POST: Edit Root service.
        [HttpPost]
        public ActionResult EditRootService(int id, ServiceView request)
        {
            try
            {
                bool isUpdated = _serviceService.UpdateRootServiceById(id, request);
                if (isUpdated)
                    return RedirectToAction("RootServiceDetails", new { id = id });
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //Get: Root Service Processing days
        public ActionResult EditRootServiceProcessingTime(int id)
        {
            try
            {
                ServiceView rootService = _serviceService.GetRootServiceProcessingTime(id);
                return View(rootService);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //POST: Edit Root service.
        [HttpPost]
        public ActionResult EditRootServiceProcessingTime(int id, ServiceView request)
        {
            try
            {
                bool isUpdated = _serviceService.UpdateRootServiceProcessingTime(id, request);
                if (isUpdated)
                    return RedirectToAction("RootServiceDetails", new { id = id });
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //Get: Root service prices
        public ActionResult EditRootServicePrices(int id)
        {
            try
            {
                ServiceView rootService = _serviceService.GetRootServiceCharges(id);
                return View(rootService);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //POST: Edit Root service.
        [HttpPost]
        public ActionResult EditRootServicePrices(int id, ServiceView request)
        {
            try
            {
                bool isUpdated = _serviceService.UpdateRootServiceCharges(id, request);
                if (isUpdated)
                    return RedirectToAction("RootServiceDetails", new { id = id });
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //POST: Edit Processing Days
        [HttpPost]
        public ActionResult EditProcessingTime(int id, ServiceView request)
        {
            if (request.ProcessingTimeView.NormalProcessingDays == null || request.ProcessingTimeView.ExpressProcessingDays == null)
                return View("EditRootService", request);
            try
            {
                bool isUpdated = _serviceService.UpdateProcessingTime(id, request);
                if (isUpdated)
                    return RedirectToAction("RootServiceDetails/" + id);
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //POST: Edit Charges
        [HttpPost]
        public ActionResult EditCharges(int id, ServiceView request)
        {
            if (request.ConvenienceChargesView.ConvenienceCharge == null || request.ConvenienceChargesView.MultiOrderExpressProcessingChargePerOrder == null ||
                request.ConvenienceChargesView.SingleOrderExpressProcessingCharge == null || request.ConvenienceChargesView.PackingCharge == null)
                return View("EditRootService", request);
            try
            {
                bool isUpdated = _serviceService.UpdateCharges(id, request);
                if (isUpdated)
                    return RedirectToAction("RootServiceDetails/" + id);
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Service/Create
        public ActionResult CreateRootService(int? categoryId)
        {
            ServiceView service = new ServiceView()
            {
                CategoryId = categoryId
            };
            return View(service);
        }

        // POST: Category/Create
        [HttpPost]
        public ActionResult CreateRootService(ServiceView request)
        {
            try
            {
                bool isCreated = _serviceService.CreateRootService(request);
                if (isCreated)
                    return RedirectToAction("ListByCategory", new { categoryId = request.CategoryId });
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: RootService/Delete
        //TODO:
        public ActionResult DeleteRootService(ServiceView request)
        {
            try
            {
                return View(request);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //POST: RootService/Delete
        [HttpPost]
        public ActionResult DeleteRootService(int id, int? categoryId)
        {
            try
            {
                bool isDeleted = _serviceService.DeleteRootService(id);
                if (isDeleted)
                    return RedirectToAction("ListByCategory", new { categoryId = categoryId });
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        #endregion

        #region SERVICE
        //GET: Service by Root Service
        public ActionResult ServicesByRoot(int? id, int? categoryId)
        {
            try
            {
                List<ServiceView> subServices = _serviceService.GetServicesByRoot(id, 0);
                ViewBag.parentid = id;
                ViewBag.categoryid = categoryId;
                return PartialView("_ServicesByRoot", subServices);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: Root service details by Id.
        public ActionResult ServiceDetails(int? id)
        {
            try
            {
                ServiceView service = _serviceService.ServiceDetails(id);
                return View(service);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: Root service details by Id.
        public ActionResult EditService(ServiceView request)
        {
            return View(request);
        }

        //POST: Edit Root service.
        [HttpPost]
        public ActionResult EditService(int id, ServiceView request)
        {
            try
            {
                bool isUpdated = _serviceService.UpdateServiceById(id, request);
                if (isUpdated)
                    return RedirectToAction("ServiceDetails/" + id);
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Service/Create
        public ActionResult CreateService(int? parentId, int? categoryId)
        {
            ServiceView service = new ServiceView()
            {
                ParentId = parentId,
                CategoryId = categoryId
            };
            return View(service);
        }

        /// <summary>
        /// 
        ///
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// 
        
        // POST: Category/Create
        [HttpPost]
        public ActionResult CreateService(ServiceView request)
        {
            try
            {
                bool isCreated = _serviceService.CreateService(request);
                if (isCreated)
                    return RedirectToAction("ServiceDetails", new { id = request.ParentId });
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

       //Append SubService
        public PartialViewResult AppendSubService ()
        {
            SubServiceView subview = new SubServiceView();
            subview.UniqueID = Guid.NewGuid();
            return PartialView("_AppendSubService",subview);
        }
        //GET: RootService/Delete
        public ActionResult DeleteService(ServiceView request)
        {
            return View(request);
        }

        //POST: RootService/Delete
        //TODO:
        [HttpPost]
        public ActionResult DeleteService(int id, int? parentId, int? categoryId)
        {
            try
            {
                bool isDeleted = _serviceService.DeleteService(id);
                if (isDeleted)
                    return RedirectToAction("ServicesByRoot", new { id = parentId, categoryId = categoryId });
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //POST: Load more services
        [HttpPost]
        public ActionResult LoadMoreServices(int? id, int? categoryId, int size)
        {
            try
            {
                List<ServiceView> listServices = _serviceService.GetServicesByRoot(id, size);
                if (listServices != null)
                {
                    ViewBag.parentid = id;
                    ViewBag.categoryid = categoryId;
                    return PartialView("_LoadMoreService", listServices);
                }
                return Json(listServices);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        #endregion

        #region SUB-SERVICE
        //GET: Sub Services by service Id.
        public ActionResult SubServicesByService(int? parentId, int? categoryId, string type)
        {
            try
            {
                List<SubServiceView> subServices = _serviceService.GetSubServicesByService(parentId, 0);
                ViewData["parentid"] = parentId;
                ViewData["categoryid"] = categoryId;
                ViewData["type"] = type;
                return PartialView("_SubServicesByService", subServices);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: Sub Services by service Id.
        [HttpPost]
        public ActionResult LoadMoreSubServicesByServiceId(int offset)
        {
            try
            {
                List<SubServiceView> subServices = _serviceService.GetSubServicesByService(4, offset);
                return null;
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: Sub Service details by id.
        public ActionResult SubServiceDetails(int id)
        {
            try
            {
                SubServiceView subService = _serviceService.GetSubServiceDetailsById(id);
                return View(subService);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: Sub Service Details by id.
        public ActionResult EditSubService(SubServiceView request)
        {
            try
            {
                return View(request);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //POST: Edit sub-service.
        [HttpPost]
        public ActionResult EditSubService(int? id, SubServiceView request)
        {
            if (!ModelState.IsValid)
                return View(request);
            try
            {
                bool isUpdated = _serviceService.UpdateSubServieById(request);
                if (isUpdated)
                    return RedirectToAction("ServiceDetails", new { id = request.ParentId });
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: Create root service
        [HttpGet]
        public ActionResult CreateSubService(SubServiceView request)
        {
            return View(request);
        }

        //POST: Create root service
        [HttpPost]
        public ActionResult CreateSubService(int? id, SubServiceView request)
        {
            try
            {
                bool isCreated = _serviceService.CreateSubServie(request);
                if (isCreated)
                    return RedirectToAction("ServiceDetails", new { id = request.ParentId });
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: Sub Service Details by id.
        public ActionResult DeleteSubService(SubServiceView request)
        {
            return View(request);
        }

        //POST: Edit sub-service.
        //TODO:
        [HttpPost]
        public ActionResult DeleteSubService(int id, int parentId)
        {
            try
            {                
                bool isDeleted = _serviceService.DeleteSubServieById(id);
                if (isDeleted)
                    return RedirectToAction("ServiceDetails", new { id = parentId });
                return View("Error");
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        #endregion

        #region HELPER METHOD
        //GET: List of service type
        public ActionResult ListServiceType()
        {
            try
            {
                var serviceList = _serviceService.ServiceList();
                return Json(serviceList, JsonRequestBehavior.AllowGet);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: List of sub-service type
        public ActionResult ListSubServiceType()
        {
            try
            {
                var subServiceList = _serviceService.SubServiceList();
                return Json(subServiceList, JsonRequestBehavior.AllowGet);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //GET: List of sub-service type
        public ActionResult SubServiceTypes()
        {
            try
            {
                var subServiceList = _serviceService.SubServiceTypes();
                return Json(subServiceList, JsonRequestBehavior.AllowGet);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: Parent list
        public ActionResult ParentServiceListByCategoryId(int categoryId)
        {
            try
            {
                var subServiceList = _serviceService.ParentServiceListByCategoryId(categoryId);
                return Json(subServiceList, JsonRequestBehavior.AllowGet);
            }
            catch (WebException ex)
            {
                _logger.Error(ex);
                var response = ex.Response as HttpWebResponse;
                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        return RedirectToAction("Logout", "Account");
                    }
                }
                return RedirectToAction("Logout", "Account");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }
        #endregion
    }
}
