﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WeDoShoesCMSAdminPanel.Filter;
using WeDoShoesCMSAdminPanel.Services;
using WeDoShoesCMSAdminPanel.ViewModel;

namespace WeDoShoesCMSAdminPanel.Controllers
{
    [AuthorizeFilter]
    public class NationStateController : Controller
    {
        private readonly NationStateService _nationStateService;
        private readonly Logger _logger;

        public NationStateController()
        {
            _nationStateService = new NationStateService();
            _logger = LogManager.GetCurrentClassLogger();
        }
        // GET: NationState
        public ActionResult Index()
        {
            try
            {
                List<NationStateView> listNationState = _nationStateService.ListNationState();
                return View(listNationState);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        // GET: NationState/Details/5
        public ActionResult Details(int id)
        {
            try
            {
                NationStateView nationState = _nationStateService.NationStateById(id);
                return View(nationState);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return View("Error");
            }
        }

        //// GET: NationState/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: NationState/Create
        //[HttpPost]
        //public ActionResult Create(NationStateView request)
        //{
        //    if (!ModelState.IsValid)
        //        return View();
        //    try
        //    {
        //        bool isCreated = _nationStateService.CreateNationState(request);
        //        if (isCreated)
        //            return RedirectToAction("Index");
        //        return View("Error");
        //    }
        //    catch
        //    {
        //        return View("Error");
        //    }
        //}

        //// GET: NationState/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    try
        //    {
        //        NationStateView nationState = _nationStateService.NationStateById(id);
        //        return View(nationState);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        return View("Error");
        //    }
        //}

        //// POST: NationState/Edit/5
        //[HttpPost]
        //public ActionResult Edit(int id, NationStateView request)
        //{
        //    if (!ModelState.IsValid)
        //        return View(request);
        //    try
        //    {
        //        bool isUpdated = _nationStateService.UpdateNationState(request);
        //        if (isUpdated)
        //            return RedirectToAction("Index");
        //        return View("Error");
        //    }
        //    catch
        //    {
        //        return View("Error");
        //    }
        //}

    }
}
