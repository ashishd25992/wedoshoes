﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WeDoShoesCMSAdminPanel.Util
{
    public static class Mapper
    {
        public static T JsonToObject<T>(string inputString)
        {
            return JsonConvert.DeserializeObject<T>(inputString);
        }

        public static string ObjectToJson<T>(T inputObject)
        {
            return JsonConvert.SerializeObject(inputObject);
        }

        public static string StreamToString(Stream stream)
        {            
            return new StreamReader(stream).ReadToEnd();
        }

        public static T JsonStreamToObject<T>(Stream stream)
        {
            return JsonConvert.DeserializeObject<T>(StreamToString(stream));
        }

    }
}