﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeDoShoesCMSAdminPanel.Models
{
    public class Accessory
    {
        [JsonProperty(PropertyName = "id", NullValueHandling = NullValueHandling.Ignore)]
        public Int32? Id { get; set; }
        [JsonProperty(PropertyName = "category_id", NullValueHandling = NullValueHandling.Ignore)]
        public Int32? CategoryId { get; set; }
        [JsonProperty(PropertyName = "name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "description", NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }
        [JsonProperty(PropertyName = "price", NullValueHandling = NullValueHandling.Ignore)]
        public AccessoryPrice AccessoryPrice { get; set; }
        [JsonProperty(PropertyName = "size_type", NullValueHandling = NullValueHandling.Ignore)]
        public Int32? SizeType;

        public bool ShouldSerializeConvenienceCharges()
        {
            return (AccessoryPrice != null);
        }
    }

    public class AccessoryPrice
    {
        [JsonProperty(PropertyName = "accessory_cost", NullValueHandling = NullValueHandling.Ignore)]
        public Double? AccessoryCost { get; set; }
    }
}