﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeDoShoesCMSAdminPanel.Models
{
    public class Login
    {        
        [JsonProperty(PropertyName = "phone", NullValueHandling = NullValueHandling.Ignore)]
        public long Phone { get; set; }
        [JsonProperty(PropertyName = "password", NullValueHandling = NullValueHandling.Ignore)]
        public string Password { get; set; }
        [JsonProperty(PropertyName = "id", NullValueHandling = NullValueHandling.Ignore)]
        public int? Id { get; set; }
        [JsonProperty(PropertyName = "email_id", NullValueHandling = NullValueHandling.Ignore)]
        public string Email { get; set; }
        [JsonProperty(PropertyName = "name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "gender", NullValueHandling = NullValueHandling.Ignore)]
        public string Gender { get; set; }
        [JsonProperty(PropertyName = "image_url", NullValueHandling = NullValueHandling.Ignore)]
        public string ImageUrl { get; set; }
        [JsonProperty(PropertyName = "referral_code", NullValueHandling = NullValueHandling.Ignore)]
        public string ReferralCode { get; set; }
        [JsonProperty(PropertyName = "type", NullValueHandling = NullValueHandling.Ignore)]
        public int? Type { get; set; }
        [JsonProperty(PropertyName = "is_active", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsActive { get; set; }        
    }
}