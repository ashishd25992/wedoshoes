﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeDoShoesCMSAdminPanel.Models
{
    public class WeDoShoesService
    {
        public WeDoShoesService()
        {
            ListPrice = new List<Price>();
            ConvenienceCharges = new ConvenienceCharges();
            ProcessingTime = new ProcessingTime();
            ListSubService = new List<SubService>();
        }
        [JsonProperty(PropertyName = "id", NullValueHandling = NullValueHandling.Ignore)]
        public Int32? Id { get; set; }
        [JsonProperty(PropertyName = "category_id", NullValueHandling = NullValueHandling.Ignore)]
        public Int32? CategoryId { get; set; }
        [JsonProperty(PropertyName = "parent_id", NullValueHandling = NullValueHandling.Ignore)]
        public Int32? ParentId { get; set; }
        [JsonProperty(PropertyName = "name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "acronym", NullValueHandling = NullValueHandling.Ignore)]
        public string Acronym { get; set; }
        [JsonProperty(PropertyName = "description", NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }
        [JsonProperty(PropertyName = "type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }
        [JsonProperty(PropertyName = "has_children", NullValueHandling = NullValueHandling.Ignore)]
        public bool? HasChildren { get; set; }
        [JsonProperty(PropertyName = "convenience_charges", NullValueHandling = NullValueHandling.Ignore)]
        public ConvenienceCharges ConvenienceCharges { get; set; }
        [JsonProperty(PropertyName = "processing_time", NullValueHandling = NullValueHandling.Ignore)]
        public ProcessingTime ProcessingTime { get; set; }
        [JsonProperty(PropertyName = "prices", NullValueHandling = NullValueHandling.Ignore)]
        public List<Price> ListPrice { get; set; }
        [JsonProperty(PropertyName = "sub_services", NullValueHandling = NullValueHandling.Ignore)]
        public List<SubService> ListSubService { get; set; }

        public bool ShouldSerializeConvenienceCharges()
        {
            return (ConvenienceCharges.IsCurrentPrice != null || ConvenienceCharges.ConvenienceCharge != null || ConvenienceCharges.SingleOrderExpressProcessingCharge != null
                || ConvenienceCharges.MultiOrderExpressProcessingChargePerOrder != null || ConvenienceCharges.PackingCharge != null);
        }

        public bool ShouldSerializeProcessingTime()
        {
            return (ProcessingTime.NormalProcessingDays != null || ProcessingTime.ExpressProcessingDays != null);
        }        
    }
    public class ConvenienceCharges
    {
        [JsonProperty(PropertyName = "is_current_price", NullValueHandling = NullValueHandling.Ignore)]
        public Boolean? IsCurrentPrice { get; set; }
        [JsonProperty(PropertyName = "convenience_charge", NullValueHandling = NullValueHandling.Ignore)]
        public Double? ConvenienceCharge { get; set; }
        [JsonProperty(PropertyName = "single_order_express_processing_charge", NullValueHandling = NullValueHandling.Ignore)]
        public Double? SingleOrderExpressProcessingCharge { get; set; }
        [JsonProperty(PropertyName = "multi_order_express_processing_charge_per_order", NullValueHandling = NullValueHandling.Ignore)]
        public Double? MultiOrderExpressProcessingChargePerOrder { get; set; }
        [JsonProperty(PropertyName = "packing_charge", NullValueHandling = NullValueHandling.Ignore)]
        public Double? PackingCharge { get; set; }
    }

    public class ProcessingTime
    {
        [JsonProperty(PropertyName = "normal_processing_days", NullValueHandling = NullValueHandling.Ignore)]
        public Int32? NormalProcessingDays { get; set; }
        [JsonProperty(PropertyName = "express_processing_days", NullValueHandling = NullValueHandling.Ignore)]
        public Int32? ExpressProcessingDays { get; set; }
    }
}