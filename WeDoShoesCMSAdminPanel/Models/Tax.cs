﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeDoShoesCMSAdminPanel.Models
{
    public class Tax
    {
        [JsonProperty(PropertyName = "id", NullValueHandling = NullValueHandling.Ignore)]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "tax_rate", NullValueHandling = NullValueHandling.Ignore)]
        public double TaxRate { get; set; }
    }
}
