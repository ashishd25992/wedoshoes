﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeDoShoesCMSAdminPanel.Models
{
    public class Faq
    {
        [JsonProperty(PropertyName = "id", NullValueHandling = NullValueHandling.Ignore)]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "question", NullValueHandling = NullValueHandling.Ignore)]
        public string Question { get; set; }
        [JsonProperty(PropertyName = "answer", NullValueHandling = NullValueHandling.Ignore)]
        public string Answer { get; set; }
        [JsonProperty(PropertyName = "category_id", NullValueHandling = NullValueHandling.Ignore)]
        public int CategoryId { get; set; }
    }
}
