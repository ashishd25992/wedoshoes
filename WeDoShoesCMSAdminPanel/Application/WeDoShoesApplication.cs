﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using WeDoShoesCMSAdminPanel.Models;
using WeDoShoesCMSAdminPanel.Networks;

namespace WeDoShoesCMSAdminPanel.Util
{
    public class WeDoShoesApplication : Page
    {
        //Create Session
        public bool CreateSession(Login login, string token)
        {
            if ((login.Id == 0) || (token == null) || (login.Name == null))
                return false;

            bool isSessionCleared = ClearSession();
            if (isSessionCleared)
            {
                Session["id"] = login.Id;
                Session["name"] = login.Name;
                Session["token"] = token;
                return true;
            }
            return false;
        }

        //Clear Session
        public bool ClearSession()
        {
            if (Session["token"] != null)
                Session.Clear();
            return true;
        }

        //Get Token
        public string GetToken()
        {
            if (Session["token"] != null)
                return Session["token"].ToString();
            return null;
        }

        //Get User Id
        internal string GetUserId()
        {
            if (Session["id"].ToString() != null)
                return Session["id"].ToString();
            return null;
        }
    }
}