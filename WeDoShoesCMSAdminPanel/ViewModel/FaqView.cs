﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeDoShoesCMSAdminPanel.ViewModel
{
    public class FaqView
    {
        [Display(Name = "Faq Id")]
        public int Id { get; set; }
        [Required(ErrorMessage = "Question is Required"), Display(Name = "Question")]
        public string Question { get; set; }
        [Required(ErrorMessage = "Answer is Required"), Display(Name = "Answer")]
        public string Answer { get; set; }
        [Required(ErrorMessage = "Category id is Required"), Display(Name = "Category Id")]
        public int CategoryId { get; set; }
    }
}
