﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeDoShoesCMSAdminPanel.ViewModel
{
    public class DefectView
    {
        [Display(Name = "Defect Id")]
        public int Id { get; set; }
        [Required(ErrorMessage = "Defect is Required"), Display(Name = "Defect")]
        public string Name { get; set; }
    }
}
