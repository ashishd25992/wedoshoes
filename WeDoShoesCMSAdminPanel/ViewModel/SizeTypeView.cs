﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WeDoShoesCMSAdminPanel.ViewModel
{
    public class SizeTypeView
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is Required"), Display(Name = "Name")]
        public string Name { get; set; }
    }
}