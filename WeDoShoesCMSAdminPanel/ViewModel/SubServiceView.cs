﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WeDoShoesCMSAdminPanel.ViewModel
{
    public class SubServiceView
    {
        public Int32? Id { get; set; }
        [Display(Name="Category")]
        public Int32? CategoryId { get; set; }
        [Required(ErrorMessage="Parent is Required"), Display(Name="Parent Service")]
        public Int32? ParentId { get; set; }
        [Required(ErrorMessage="Name is Required"), Display(Name="Name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Acronym is Required"), Display(Name = "Acronym"), MaxLength(10)]
        public string Acronym { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Type is Required"), Display(Name="Type")]
        public string Type { get; set; }
        [Required(ErrorMessage = "HasChildren is Required"), Display(Name = "Has Children")]
        public bool? HasChildren { get; set; }

        public Guid UniqueID { get; set; }
    }
}