﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WeDoShoesCMSAdminPanel.ViewModel
{
    public class APIView
    {
        public int? Id { get; set; }
        [Required(ErrorMessage="Name is Required."), Display(Name="Name")]
        public string Name { get; set; }
        [Required(ErrorMessage="Display name is Required."),Display(Name="Display Name")]
        public string DisplayName { get; set; }
        [Required(ErrorMessage="Description is Required."), Display(Name="Description")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Category is Required."), Display(Name = "Category")]
        public string Category { get; set; }
        [Required(ErrorMessage="API is Required"), Display(Name="API")]
        public List<int> SelectedAPIIds { get; set; }
    }
}