﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WeDoShoesCMSAdminPanel.ViewModel
{
    public class ServiceView
    {
        public ServiceView()
        {
            ConvenienceChargesView = new ConvenienceChargesView();
            ProcessingTimeView = new ProcessingTimeView();
            ListSubServiceView = new List<SubServiceView>();
            SubServiceView = new SubServiceView();
            ListPriceView = new List<PriceView>();
            PriceView = new PriceView();
        }
        public Int32? Id { get; set; }
        [Required(ErrorMessage = "Category is Required"), Display(Name = "Category")]
        public Int32? CategoryId { get; set; }
        [Required(ErrorMessage = "Parent is Required"), Display(Name = "Parent Service")]
        public Int32? ParentId { get; set; }
        [Required(ErrorMessage = "Name is Required"), Display(Name = "Name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Acronym is Required"), Display(Name = "Acronym"), MaxLength(10)]
        public string Acronym { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Type")]
        public string Type { get; set; }
        [Display(Name = "Has Children")]
        public bool HasChildren { get; set; }
        public ConvenienceChargesView ConvenienceChargesView { get; set; }
        public ProcessingTimeView ProcessingTimeView { get; set; }
        public List<SubServiceView> ListSubServiceView { get; set; }
        public SubServiceView SubServiceView { get; set; }
        public List<PriceView> ListPriceView { get; set; }
        public PriceView PriceView { get; set; }
    }

    public class ConvenienceChargesView
    {
        public Boolean? IsCurrentPrice { get; set; }
        [Required(ErrorMessage = "Convenience Charge is Required"), Display(Name = "Convenience Charge")]
        public Double? ConvenienceCharge { get; set; }
        [Required(ErrorMessage = "Single Order Express Processing Charge is Required"), Display(Name = "Single Order EPC")]
        public Double? SingleOrderExpressProcessingCharge { get; set; }
        [Required(ErrorMessage = "Multi Order Express Processing Charge Per Order is Required"), Display(Name = "Multi Order EPC/Order")]
        public Double? MultiOrderExpressProcessingChargePerOrder { get; set; }
        [Required(ErrorMessage = "Packing Charge is Required"), Display(Name = "Packing Charge")]
        public Double? PackingCharge { get; set; }
    }

    public class ProcessingTimeView
    {
        [Required(ErrorMessage = "Normal Processing Days is Required"), Display(Name = "Normal Process Days")]
        public Int32? NormalProcessingDays { get; set; }
        [Required(ErrorMessage = "Express Processing Days is Required"), Display(Name = "Express Process Days")]
        public Int32? ExpressProcessingDays { get; set; }
    }
}