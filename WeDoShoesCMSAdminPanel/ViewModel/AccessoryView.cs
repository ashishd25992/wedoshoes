﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WeDoShoesCMSAdminPanel.ViewModel
{
    public class AccessoryView
    {
        public AccessoryView()
        {
            AccessoryPriceView = new AccessoryPriceView();
        }
        [Display(Name="Accessory")]
        public Int32? Id { get; set; }
        [Required(ErrorMessage = "Category is Required"), Display(Name = "Category")]
        public Int32? CategoryId { get; set; }
        [Required(ErrorMessage = "Name is Required"), Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        public AccessoryPriceView AccessoryPriceView { get; set; }        
        [Display(Name = "Size")]
        public Int32? SizeType { get; set; }
    }

    public class AccessoryPriceView
    {
        [Required(ErrorMessage = "Cost is Required"), Display(Name = "Accessory Cost")]
        public Double? AccessoryCost { get; set; }
    }
}