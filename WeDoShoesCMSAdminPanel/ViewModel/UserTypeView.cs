﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace WeDoShoesCMSAdminPanel.ViewModel
{
    public class UserTypeView
    {
        [JsonProperty(PropertyName = "id", NullValueHandling = NullValueHandling.Ignore)]
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is Required"), Display(Name = "Name")]
        [JsonProperty(PropertyName = "name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }
        [Required(ErrorMessage = "User Type is Required"), Display(Name = "User Type")]
        [JsonProperty(NullValueHandling =NullValueHandling.Ignore)]
        public List<int> SelectedUserType { get; set; }
    }
}