﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeDoShoesCMSAdminPanel.ViewModel
{
    public class SearchDefectView
    {
        [Display(Name="Defect Name")]
        public string Keyword { get; set; }
    }
}
