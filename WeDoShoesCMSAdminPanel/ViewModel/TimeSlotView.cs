﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeDoShoesCMSAdminPanel.ViewModel
{
    public class TimeSlotView
    {
        [Display(Name = "Time Slot")]
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is Required"), Display(Name = "Name")]
        public string Name { get; set; }
    }
}
