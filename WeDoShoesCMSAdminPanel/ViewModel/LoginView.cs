﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WeDoShoesCMSAdminPanel.ViewModel
{
    public class LoginView
    {
        [Required(ErrorMessage = "Mobile number is Required"), RegularExpression(@"^(\d{12})$", ErrorMessage = "Please enter valid mobile number."), Display(Name = "Phone")]
        public long MobileNo { get; set; }
        [Required, MinLength(6), MaxLength(20), Display(Name = "Password")]
        public string Password { get; set; }
    }
}