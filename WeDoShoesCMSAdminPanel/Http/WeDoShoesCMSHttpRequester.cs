﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using WeDoShoesCMSAdminPanel.Models;
using WeDoShoesCMSAdminPanel.Networks;

namespace WeDoShoesCMSAdminPanel.Util
{
    public class WeDoShoesCMSHttpRequester
    {
        private static readonly string APPLICATION_JSON = "application/json";
        private readonly WeDoShoesApplication _weDoShoesApplication;
        private readonly Logger _logger;

        public WeDoShoesCMSHttpRequester()
        {
            _weDoShoesApplication = new WeDoShoesApplication();
            _logger = LogManager.GetCurrentClassLogger();
        }

        //List Accessories
        internal List<Accessory> ListAccessories(int offset)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.LIST_OF_ACCESSORIES, WeDoShoesCMSWebServices.BASE_URI, offset);
            var headers = GetHaders(true);
            string token = string.Empty;
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return Mapper.JsonStreamToObject<List<Accessory>>(httpWebResponse.GetResponseStream());
            return null;
        }

        //List Accessories by category
        internal List<Accessory> ListAccessoriesByCategory(int? categoryId, int offset)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.ACCESSORIES_BY_CATEGORY_ID, WeDoShoesCMSWebServices.BASE_URI, offset, categoryId);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return Mapper.JsonStreamToObject<List<Accessory>>(httpWebResponse.GetResponseStream());
            return null;
        }

        //Accessory Details
        internal Accessory AccessoryDetails(int id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.GET_ACCESSORY_BY_ID, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return Mapper.JsonStreamToObject<Accessory>(httpWebResponse.GetResponseStream());
            return null;
        }

        //Create Accessory
        internal bool CreateAccessory(Accessory accessory)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.CREATE_ACCESSORY, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<Accessory>(accessory);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return true;
            return false;
        }

        //Update Accessory
        internal bool UpdateAccessory(Accessory accessory, int? id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.UPDATE_ACCESSORY, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<Accessory>(accessory);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //Update Accessory Cost
        internal bool UpdateCost(AccessoryPrice accessoryPrice, int? id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.UPDATE_COST_BY_SIZE_TYPE_ID, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<AccessoryPrice>(accessoryPrice);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //TODO:
        internal bool DeleteAccessory(ViewModel.AccessoryView request)
        {
            throw new NotImplementedException();
        }

        //List Brands
        internal List<Brands> ListBrands()
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.LIST_OF_BRANDS, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<List<Brands>>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //List brands by category
        internal List<Brands> ListBrandsByCategory(int? categoryId)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.LIST_OF_BRANDS_BY_CATEGORY, WeDoShoesCMSWebServices.BASE_URI, categoryId);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<List<Brands>>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Brand Details
        internal Brands BrandDetails(int id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.GET_BRAND_BY_ID, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<Brands>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Create Brand
        internal bool CreateBrand(Brands brand)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.CREATE_BRAND, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<Brands>(brand);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return true;
            return false;
        }

        //Update Brand
        internal bool UpdateBrand(Brands brand, int? id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.UPDATE_BRAND, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<Brands>(brand);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //TODO:
        internal bool DeleteBrand(int id)
        {
            throw new NotImplementedException();
        }

        //List Category
        internal List<Category> ListCategory()
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.LIST_OF_CATEGORIES, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<List<Category>>(httpWebResponse.GetResponseStream());            
            return null;

        }

        //Category Details
        internal Category CategoryDetails(int id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.CATEGORY, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<Category>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Create Category
        internal bool CreateCategory(Category category)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.CREATE_CATEGORY, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<Category>(category);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return true;
            return false;
        }

        //Update Category
        internal bool UpdateCategory(Category category, int id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.UPDATE_CATEGORY, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<Category>(category);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //Delete Category
        internal bool DeleteCategory(int id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.DELETE_CATEGORY, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Delete(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return true;
            return false;
        }

        //List Product
        internal List<Product> ListProduct(int offset)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.PRODUCT_BY_CATEGORY_ID, WeDoShoesCMSWebServices.BASE_URI, 1, offset);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<List<Product>>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Product By Category
        internal List<Product> ProductByCategory(int? categoryId, int? offset)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.PRODUCT_BY_CATEGORY_ID, WeDoShoesCMSWebServices.BASE_URI, categoryId, offset);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<List<Product>>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Product details
        internal Product ProductDetails(int id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.PRODUCT_BY_ID, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<Product>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //List of services of a product
        internal List<WeDoShoesService> ListServiceByProduct(int productId)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.SERVICE_BY_PRODUCT, WeDoShoesCMSWebServices.BASE_URI, productId);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<List<WeDoShoesService>>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //List of brands by product
        internal List<Brands> ListBrandByProduct(int? productId)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.BRAND_BY_PRODUCT, WeDoShoesCMSWebServices.BASE_URI, productId);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<List<Brands>>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Add brand in a product
        internal bool AddBrandInProduct(List<int> list, int? id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.ADD_BRAND_TO_PRODUCT, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<List<int>>(list);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //Remove brands from a product
        internal bool RemoveBrandFromProduct(int id, int productId)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.REMOVE_BRAND_FROM_PRODUCT, WeDoShoesCMSWebServices.BASE_URI, productId, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Delete(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return true;
            return false;
        }

        //List size of a product
        internal List<SizeType> ListSizeOfAProduct(int productId)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.SIZE_BY_PRODUCT, WeDoShoesCMSWebServices.BASE_URI, productId);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<List<SizeType>>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Remove size from a product
        internal bool RemoveSizeFromProduct(int id, int productId)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.REMOVE_SIZE_FROM_PRODUCT, WeDoShoesCMSWebServices.BASE_URI, productId, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Delete(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return true;
            return false;
        }

        //List of service by it's type and category
        internal List<WeDoShoesService> ListServiceByTypeAndCategory(string type, int categoryId)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.SERVICE_BY_CATEGORY_AND_TYPE, WeDoShoesCMSWebServices.BASE_URI, type, categoryId);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)           
                return Mapper.JsonStreamToObject<List<WeDoShoesService>>(httpWebResponse.GetResponseStream());           
            return null;
        }

        //List accessories minimum info by category
        internal List<Accessory> ListAccessoriesMinInfoByCategory(int categoryId)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.ACCESSORY_MIN_INFO_BY_CATEGORY, WeDoShoesCMSWebServices.BASE_URI, categoryId);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<List<Accessory>>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Add service to a product
        internal bool AddServiceToProduct(List<WeDoShoesService> listService, int? id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.ADD_SERVICE_TO_PRODUCT, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<List<WeDoShoesService>>(listService);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //Create Product
        internal bool CreateProduct(Product product)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.CREATE_PRODUCT, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<Product>(product);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return true;
            return false;
        }

        //Update product
        internal bool UpdateProduct(Product product, int? id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.UPDATE_PRODUCT, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<Product>(product);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //List services
        internal List<WeDoShoesService> ListRootServices()
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.LIST_OF_ROOT_SERVICE, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<List<WeDoShoesService>>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //List root services by category
        internal List<WeDoShoesService> ListRootServiceByCategory(int id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.ROOT_SERVICE_BY_CATEGORY_ID, WeDoShoesCMSWebServices.BASE_URI, 0, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<List<WeDoShoesService>>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Root service details
        internal WeDoShoesService RootServiceDetails(int id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.ROOT_SERVICE_BY_SERVICE_ID, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<WeDoShoesService>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Update root service
        internal bool UpdateRootService(int id, WeDoShoesService service)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.UPDATE_ROOT_SERVICE, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<WeDoShoesService>(service);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //Get root service processing time
        internal WeDoShoesService RootServiceProcessingTime(int id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.ROOT_SERVICE_BY_SERVICE_ID, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<WeDoShoesService>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Update root service processing time.
        internal bool UpdateRootServiceProcessingTime(int id, ProcessingTime processingTime)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.UPDATE_PROCESSING_TIME, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<ProcessingTime>(processingTime);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //Get root service price
        internal WeDoShoesService RootServiceCharges(int id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.ROOT_SERVICE_BY_SERVICE_ID, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<WeDoShoesService>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Update root service charges
        internal bool UpdateRootServiceCharges(int id, ConvenienceCharges convenienceCharges)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.UPDATE_CHARGES, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<ConvenienceCharges>(convenienceCharges);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //Update root service processing time
        internal bool UpdateProcessingTime(int id, ProcessingTime processingTime)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.UPDATE_PROCESSING_TIME, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<ProcessingTime>(processingTime);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //Create root services
        internal bool CreateRootService(WeDoShoesService service)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.CREATE_ROOT_SERVICE, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<WeDoShoesService>(service);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return true;
            return false;
        }

        //Update root service charges
        internal bool UpdateCharges(int id, ConvenienceCharges convenienceCharges)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.UPDATE_CHARGES, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<ConvenienceCharges>(convenienceCharges);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //Delete root service
        internal bool DeleteRootService(int id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.DELETE_ROOT_SERVICE, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Delete(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //List of services by root
        internal List<WeDoShoesService> ListServiceByRoot(int? id, int offset)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.SERVICE_BY_ROOT, WeDoShoesCMSWebServices.BASE_URI, offset, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK || httpWebResponse.StatusCode == HttpStatusCode.NoContent)            
                return Mapper.JsonStreamToObject<List<WeDoShoesService>>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Service details
        internal WeDoShoesService ServiceDetails(int? id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.SERVICE_BY_ID, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<WeDoShoesService>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Update service
        internal bool UpdateService(int? id, WeDoShoesService service)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.UPDATE_SERVICE, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<WeDoShoesService>(service);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //Create Service
        internal bool CreateService(WeDoShoesService service)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.CREATE_SERVICE, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<WeDoShoesService>(service);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return true;
            return false;
        }

        //List of sub-services by service
        internal List<WeDoShoesService> ListSubServiceByService(int? id, int count)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.SUB_SERVICE_BY_PARENT_ID, WeDoShoesCMSWebServices.BASE_URI, count, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<List<WeDoShoesService>>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Get sub-service details.
        internal WeDoShoesService SubServiceDetails(int id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.ROOT_SERVICE_BY_SERVICE_ID, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<WeDoShoesService>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Update sub-service
        internal bool UpdateSubService(int? id, SubService subService)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.UPDATE_ROOT_SERVICE, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<SubService>(subService);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //Create sub-service
        internal bool CreateSubService(SubService subService)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.CREATE_SUB_SERVICE, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<SubService>(subService);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return true;
            return false;
        }

        //List of parent service by category
        internal List<WeDoShoesService> ListParentServiceByCategory(int? categoryId)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.ROOT_SERVICE_BY_CATEGORY_ID_MINIMAL_INFO, WeDoShoesCMSWebServices.BASE_URI, categoryId);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<List<WeDoShoesService>>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //List size type
        internal List<SizeType> ListSize()
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.LIST_SIZE_TYPE, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<List<SizeType>>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Get size details
        internal SizeType SizeDetails(int id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.SIZE_TYPE_BY_ID, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<SizeType>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Create size
        internal bool CeateSize(SizeType sizeType)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.CREATE_SIZE_TYPE, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<SizeType>(sizeType);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return true;
            return false;
        }

        //Update size
        internal bool UpdateSize(int id, SizeType sizeType)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.UPDATE_SIZE_TYPE_BY_ID, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<SizeType>(sizeType);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //List Contact Mode
        internal List<ContactMode> ListContactMode()
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.LIST_CONTACT_MODE, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return Mapper.JsonStreamToObject<List<ContactMode>>(httpWebResponse.GetResponseStream());
            return null;
        }

        //Create Contact Mode
        internal bool CreateContactMode(ContactMode contactMode)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.CREATE_CONTACT_MODE, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<ContactMode>(contactMode);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return true;
            return false;
        }

        //Get Contact Mode Details
        internal ContactMode ContactModeDetails(int id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.CONTACT_MODE_BY_ID, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return Mapper.JsonStreamToObject<ContactMode>(httpWebResponse.GetResponseStream());
            return null;
        }

        internal bool UpdateContactMode(int id, ContactMode contactMode)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.UPDATE_CONTACT_MODE, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<ContactMode>(contactMode);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //Faq Category List
        internal List<FaqCategory> ListFaqCategory()
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.LIST_FAQ_CATEGORY, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return Mapper.JsonStreamToObject<List<FaqCategory>>(httpWebResponse.GetResponseStream());
            return null;
        }

        //Faq List By Category
        internal List<Faq> ListFaq(int id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.LIST_FAQ, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return Mapper.JsonStreamToObject<List<Faq>>(httpWebResponse.GetResponseStream());
            return null;
        }

        internal bool CreateFaqCategory(FaqCategory faqCategory)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.CREATE_FAQ_CATEGORY, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<FaqCategory>(faqCategory);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return true;
            return false;
        }

        internal FaqCategory FaqCategoryDetails(int id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.FAQ_CATEGORY_BY_ID, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return Mapper.JsonStreamToObject<FaqCategory>(httpWebResponse.GetResponseStream());
            return null;
        }

        internal bool UpdateFaqCategory(int? id, FaqCategory faqCategory)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.UPDATE_FAQ_CATEGORY, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<FaqCategory>(faqCategory);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        internal bool CreateFaq(Faq faq)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.CREATE_FAQ, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<Faq>(faq);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return true;
            return false;
        }


        internal bool UpdateFaq(int id, Faq faq)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.UPDATE_FAQ, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<Faq>(faq);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }


        internal Tax GetTax()
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.GET_Tax, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return Mapper.JsonStreamToObject<Tax>(httpWebResponse.GetResponseStream());
            return null;
        }

        internal bool CreateTax(Tax tax)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.CREATE_Tax, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<Tax>(tax);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return true;
            return false;
        }

        internal bool UpdateTax(Tax tax)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.UPDATE_Tax, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<Tax>(tax);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        internal List<TimeSlot> ListTimeSlot()
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.LIST_Time_Slot, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return Mapper.JsonStreamToObject<List<TimeSlot>>(httpWebResponse.GetResponseStream());
            return null;
        }

        internal TimeSlot TimeSlotDetails(int id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.CONTACT_MODE_BY_ID, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return Mapper.JsonStreamToObject<TimeSlot>(httpWebResponse.GetResponseStream());
            return null;
        }

        #region Helper Method
        //Get all required headers.
        internal Dictionary<string, string> GetHaders(bool isSecuredApi)
        {
            Dictionary<string, string> headers = new Dictionary<string, string>();
            if (isSecuredApi)
            {
                headers.Add("Authorization", _weDoShoesApplication.GetToken());
                headers.Add("X-UserID", _weDoShoesApplication.GetUserId());
            }
            return headers;
        }

        #endregion                     

    
        internal bool CreateTimeSlot(TimeSlot timeSlot)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.CREATE_Time_Slot, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<TimeSlot>(timeSlot);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return true;
            return false;
        }

        internal bool UpdateTimeSlot(int id, TimeSlot timeSlot)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.UPDATE_Time_Slot, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<TimeSlot>(timeSlot);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        internal List<NationState> ListNationState()
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.LIST_NATION_STATE, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return Mapper.JsonStreamToObject<List<NationState>>(httpWebResponse.GetResponseStream());
            return null;
        }

        internal NationState NationStateDetails(int id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.NATION_STATE_BY_ID, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return Mapper.JsonStreamToObject<NationState>(httpWebResponse.GetResponseStream());
            return null;
        }

        internal bool CreateNationState(NationState nationState)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.CREATE_NATION_STATE, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<NationState>(nationState);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return true;
            return false;
        }

        internal bool UpdateNationState(int id, NationState nationState)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.UPDATE_NATION_STATE, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<NationState>(nationState);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        internal List<Defect> ListDefectView()
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.LIST_DEFECT, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return Mapper.JsonStreamToObject<List<Defect>>(httpWebResponse.GetResponseStream());
            return null;
        }

        internal Defect DefectDetails(int id)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.DEFECT_BY_ID, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return Mapper.JsonStreamToObject<Defect>(httpWebResponse.GetResponseStream());
            return null;
        }

        internal bool CreateDefect(Defect defect)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.CREATE_DEFECT, WeDoShoesCMSWebServices.BASE_URI);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<Defect>(defect);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return true;
            return false;
        }

        internal bool UpdateDefect(int id, Defect defect)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.UPDATE_DEFECT, WeDoShoesCMSWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<Defect>(defect);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        internal List<Defect> DefectByName(string keyword)
        {
            string requestUrl = string.Format(WeDoShoesCMSWebServices.DEFECT_BY_NAME, WeDoShoesCMSWebServices.BASE_URI, keyword);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return Mapper.JsonStreamToObject<List<Defect>>(httpWebResponse.GetResponseStream());
            return null;
        }
    }
}