﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Authentication;
using System.Web;
using WeDoShoesCMSAdminPanel.Models;
using WeDoShoesCMSAdminPanel.Networks;

namespace WeDoShoesCMSAdminPanel.Util
{
    public class WeDoShoesCoreHttpRequester
    {
        private static readonly string APPLICATION_JSON = "application/json";
        private readonly WeDoShoesApplication _weDoShoesApplication;
        public WeDoShoesCoreHttpRequester()
        {
            _weDoShoesApplication = new WeDoShoesApplication();
        }

        //Login
        internal Login Login(long phone, string password)
        {
            var requestUrl = string.Format(WeDoShoesCoreWebServices.LOGIN, WeDoShoesCoreWebServices.BASE_URI);
            var headers = GetHaders(false);
            headers.Add("Device-Type", "WEB");
            Login login = new Login()
            {
                Phone = phone,
                Password = password
            };
            var requestPayload = Mapper.ObjectToJson<Login>(login);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            Login loginResponse = Mapper.JsonStreamToObject<Login>(httpWebResponse.GetResponseStream());
            bool isSessionCreated = _weDoShoesApplication.CreateSession(loginResponse, httpWebResponse.GetResponseHeader("Authorization"));
            if (isSessionCreated)
                return loginResponse;
            return null;
        }

        //Logout
        internal bool Logout()
        {
            return _weDoShoesApplication.ClearSession();
        }

        //List of user types
        internal List<UserType> ListUserType()
        {
            string requestUrl = string.Format(WeDoShoesCoreWebServices.LIST_USER_TYPE, WeDoShoesCoreWebServices.BASE_URI);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<List<UserType>>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Get user type details
        internal UserType UserTypeDetails(int id)
        {
            string requestUrl = string.Format(WeDoShoesCoreWebServices.GET_USER_TYPE, WeDoShoesCoreWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<UserType>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Create user type
        internal bool CreateUserType(UserType userType)
        {
            string requestUrl = string.Format(WeDoShoesCoreWebServices.CREATE_USER_TYPE, WeDoShoesCoreWebServices.BASE_URI);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<UserType>(userType);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return true;
            return false;
        }

        //Update user type
        internal bool UpdateUserType(int id, UserType userType)
        {
            string requestUrl = string.Format(WeDoShoesCoreWebServices.UPDATE_USER_TYPE, WeDoShoesCoreWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<UserType>(userType);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //Delete user type
        internal bool DeleteUserType(int id)
        {
            string requestUrl = string.Format(WeDoShoesCoreWebServices.DELETE_USER_TYPE, WeDoShoesCoreWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Delete(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //List of apis
        internal List<API> ListAPI(int? offset)
        {
            string requestUrl = string.Format(WeDoShoesCoreWebServices.LIST_API, WeDoShoesCoreWebServices.BASE_URI, offset);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<List<API>>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Get api details
        internal API APIDetails(int id)
        {
            string requestUrl = string.Format(WeDoShoesCoreWebServices.API_DETAILS, WeDoShoesCoreWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<API>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Create api
        internal bool CreateAPI(API api)
        {
            string requestUrl = string.Format(WeDoShoesCoreWebServices.CREATE_API, WeDoShoesCoreWebServices.BASE_URI);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<API>(api);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return true;
            return false;
        }

        //Update API
        internal bool UpdateAPI(int id, API api)
        {
            string requestUrl = string.Format(WeDoShoesCoreWebServices.UPDATE_API, WeDoShoesCoreWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<API>(api);
            var httpWebResponse = WeDoShoesHttpClient.Put(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //List of api by user type
        internal List<API> ListOfAPIByUserType(int id, int? offset)
        {
            string requestUrl = string.Format(WeDoShoesCoreWebServices.LIST_API_BY_USER_TYPE, WeDoShoesCoreWebServices.BASE_URI, offset, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<List<API>>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //List user type by API
        internal List<UserType> ListUserTypeByAPI(int id)
        {
            string requestUrl = string.Format(WeDoShoesCoreWebServices.LIST_USER_TYPE_BY_API, WeDoShoesCoreWebServices.BASE_URI, 0, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<List<UserType>>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //List of user types not in api
        internal List<UserType> ListUserNotInAPI(int id)
        {
            string requestUrl = string.Format(WeDoShoesCoreWebServices.LIST_USER_TYPE_NOT_IN_API, WeDoShoesCoreWebServices.BASE_URI, 0, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<List<UserType>>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Add User type in api for permission
        internal bool AddUserType(int id, UserType userType)
        {
            string requestUrl = string.Format(WeDoShoesCoreWebServices.ADD_USER_TYPE_IN_API, WeDoShoesCoreWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<List<int>>(userType.SelectedUserTypeIds);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //List user type by api
        internal List<API> ListAPINotInUserType(int? id)
        {
            string requestUrl = string.Format(WeDoShoesCoreWebServices.LIST_API_NOT_IN_USER_TYPE, WeDoShoesCoreWebServices.BASE_URI, 0, id);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Get(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)            
                return Mapper.JsonStreamToObject<List<API>>(httpWebResponse.GetResponseStream());            
            return null;
        }

        //Add API permission for user type
        internal bool AddAPIPermission(int? id, API api)
        {
            string requestUrl = string.Format(WeDoShoesCoreWebServices.ADD_API_IN_USER_TYPE, WeDoShoesCoreWebServices.BASE_URI, id);
            var headers = GetHaders(true);
            var requestPayload = Mapper.ObjectToJson<List<int>>(api.SelectedAPIIds);
            var httpWebResponse = WeDoShoesHttpClient.Post(requestUrl, requestPayload, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }

        //Revoke api permission
        internal bool RevokeAPIPermission(int userTypeId, int? apiId)
        {
            string requestUrl = string.Format(WeDoShoesCoreWebServices.REVOKE_API_PERMISSION, WeDoShoesCoreWebServices.BASE_URI, apiId, userTypeId);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Delete(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                return true;
            return false;
        }

        #region Helper Method
        //Get all required headers.
        internal Dictionary<string, string> GetHaders(bool isSecuredApi)
        {
            Dictionary<string, string> headers = new Dictionary<string, string>();
            if (isSecuredApi)
            {
                headers.Add("Authorization", _weDoShoesApplication.GetToken());
                headers.Add("X-UserID", _weDoShoesApplication.GetUserId());
            }
            return headers;
        }
        #endregion

        //Revoke user permission
        internal bool RevokeUserPermission(int userTypeId, int? apiId)
        {
            string requestUrl = string.Format(WeDoShoesCoreWebServices.REVOKE_USER_PERMISSION, WeDoShoesCoreWebServices.BASE_URI, userTypeId, apiId);
            var headers = GetHaders(true);
            var httpWebResponse = WeDoShoesHttpClient.Delete(requestUrl, headers, APPLICATION_JSON, APPLICATION_JSON);
            if (httpWebResponse.StatusCode == HttpStatusCode.NoContent)
                return true;
            return false;
        }
    }
}