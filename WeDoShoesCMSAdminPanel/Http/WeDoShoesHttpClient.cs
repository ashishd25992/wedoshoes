﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;

namespace WeDoShoesCMSAdminPanel.Util
{
    #region certificate policy
    public class MyPolicy : ICertificatePolicy
    {
        public bool CheckValidationResult(
              ServicePoint srvPoint
            , X509Certificate certificate
            , WebRequest request
            , int certificateProblem)
        {

            //Return True to force the certificate to be accepted.
            return true;

        } // end CheckValidationResult
    } // class MyPolicy
    #endregion
    public static class WeDoShoesHttpClient
    {

        static WeDoShoesHttpClient()
        {

        }
        #region Methods
        private const string GET = "GET";
        private const string POST = "POST";
        private const string PUT = "PUT";
        private const string DELETE = "DELETE";
        #endregion

        #region Content type
        private const string CONTENT_TYPE = "application/json";
        private const string ACCEPT = "application/json";
        #endregion

        #region Certificates
        //public static void AttachClientCertificate(HttpWebRequest request)
        //{
        //    var filepath = System.Web.HttpContext.Current.Server.MapPath("~/Certificates/certificate.crt");
        //    //X509Certificate certificate = X509Certificate.CreateFromCertFile(filepath);
        //    X509Certificate2 certificate = new X509Certificate2(filepath);
        //    request.ClientCertificates.Add(certificate);
        //    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        //}
        #endregion

        /// <summary>
        /// Get data
        /// </summary>
        /// <param name="requestUrl"></param>
        /// <param name="contentType"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public static HttpWebResponse Get(string requestUrl, Dictionary<string, string> headers, string contentType, string acceptType)
        {
            //Create http Request
            HttpWebRequest httprequest = WebRequest.Create(requestUrl) as HttpWebRequest;
            //Add all headers.
            httprequest.ContentType = contentType;
            httprequest.Accept = acceptType;
            httprequest.UserAgent = System.Web.HttpContext.Current.Request.UserAgent.ToString();
            foreach (var header in headers)
                httprequest.Headers.Add(header.Key, header.Value);
            httprequest.Method = GET;
            //Get response
            HttpWebResponse response = httprequest.GetResponse() as HttpWebResponse;
            return response;
        }

        /// <summary>
        /// Post to server
        /// </summary>
        /// <param name="requestUrl">Request Url</param>
        /// <param name="request">Request Body</param>
        /// <param name="headers">Header</param>
        /// <param name="contentType">Content Type</param>
        /// <param name="acceptType">Accept Type</param>
        /// <returns></returns>
        public static HttpWebResponse Post(string requestUrl, string request, Dictionary<string, string> headers, string contentType, string acceptType)
        {
            //Create http request url.
            HttpWebRequest httprequest = WebRequest.Create(requestUrl) as HttpWebRequest;
            //Add all headers.
           
            httprequest.ContentType = contentType;
            httprequest.Accept = acceptType;
            httprequest.UserAgent = System.Web.HttpContext.Current.Request.UserAgent.ToString();
            foreach (var header in headers)
                httprequest.Headers.Add(header.Key, header.Value);
            //Set Request method.
            
            httprequest.Method = POST;// "POST";
                                      
            //Encode request into byte array
            Byte[] bt = Encoding.UTF8.GetBytes(request);
     
            Stream st = httprequest.GetRequestStream();
            st.Write(bt, 0, bt.Length);
            st.Close();
            httprequest.ClientCertificates.Add(new X509Certificate());
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            HttpWebResponse response = httprequest.GetResponse() as HttpWebResponse;
            return response;
        }

        /// <summary>
        /// Put the Information on server
        /// </summary>
        /// <param name="requestUrl">input url where want to post information</param>
        /// <param name="request">input data to post on server</param>
        /// <param name="ContentType">input the content type</param>
        /// <returns></returns>
        public static HttpWebResponse Put(string requestUrl, string request, Dictionary<string, string> headers, string contentType, string acceptType)
        {
            HttpWebRequest httprequest = WebRequest.Create(requestUrl) as HttpWebRequest;
            //Add all headers.
            httprequest.ContentType = contentType;
            httprequest.Accept = acceptType;
            httprequest.UserAgent = System.Web.HttpContext.Current.Request.UserAgent.ToString();
            foreach (var header in headers)
                httprequest.Headers.Add(header.Key, header.Value);
            httprequest.Method = PUT;// "PUT";                           
            //encode request into byte array
            Byte[] bt = System.Text.Encoding.UTF8.GetBytes(request);
            Stream st = httprequest.GetRequestStream();
            st.Write(bt, 0, bt.Length);
            st.Close();

            //Get Response
            HttpWebResponse response = httprequest.GetResponse() as HttpWebResponse;
            return response;

        }

        /// <summary>
        /// Delete resources
        /// </summary>
        /// <param name="url"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public static HttpWebResponse Delete(string requestUrl, Dictionary<string, string> headers, string contentType, string acceptType)
        {
            HttpWebRequest httprequest = WebRequest.Create(requestUrl) as HttpWebRequest;
            //Add all headers.
            httprequest.ContentType = contentType;
            httprequest.Accept = acceptType;
            httprequest.UserAgent = System.Web.HttpContext.Current.Request.UserAgent.ToString();
            foreach (var header in headers)
                httprequest.Headers.Add(header.Key, header.Value);
            httprequest.Method = DELETE;// "DELETE";           

            //Get Response
            HttpWebResponse response = httprequest.GetResponse() as HttpWebResponse;
            return response;
        }
    }
}